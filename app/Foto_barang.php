<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Foto_barang extends Model
{
    protected $fillable = ['gambar','barang_id'];
    public $timestamps = true;
    //function
    public function Barang()
    {
    	return $this->belongsTo('App/Foto_barang','barang_id');
    }
}
