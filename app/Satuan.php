<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Satuan extends Model
{
    protected $fillable = ['nama_satuan','nilai_satuan'];
    public $timestamps = true;
    //
    public function Barang(){
    	return $this->hasOne('App\Barang','satuan_id');
    }
}
