<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
   	protected $fillable = ['nama_supplier','nama_perusahaan','alamat','no_telp'];
   	public $timestamps = true;
   	//
   	public function Barang(){
    	return $this->hasOne('App\Barang','supplier_id');
    }
    public function Barang_Masuk(){
    	return $this->hasMany('App\barang_masuk','supplier_id');
    }

}
