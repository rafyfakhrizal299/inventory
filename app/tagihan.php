<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tagihan extends Model
{
   	protected $table = 'tagihans';
	protected $fillable = array('order_detail_id','total_pembayaran','sisa_pembayaran','status');
	public $timestamp = true;

   	public function Order_detail() {
		return $this->belongsTo('App\order_detail', 'order_detail_id');
	}
}
