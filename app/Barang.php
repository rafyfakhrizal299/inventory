<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
   	protected $fillable = ['nama_barang','ukuran','asal','harga_beli','harga_jual','stok','satuan_id','merk_id','kategori_id','supplier_id'];
   	public $timestamps = true;
   	//function tamu
   	public function Foto_gambar(){
   		return $this->hasMany('App\Foto_gambar','barang_id');
   	}
      public function Barang_masuk(){
         return $this->hasMany('App\barang_masuk','barang_id');
      }
      public function Barang_keluar(){
         return $this->hasMany('App\Barang_keluar','barang_id');
      }
      public function Order(){
         return $this->hasMany('App\Order','barang_id');
      }
   	public function Satuan(){
   		return $this->belongsTo('App\Satuan','satuan_id');
   	}
   	public function Merk(){
   		return $this->belongsTo('App\Merk','merk_id');
   	}
   	public function Supplier(){
   		return $this->belongsTo('App\Supplier','supplier_id');
   	}
   	public function Kategori(){
   		return $this->belongsTo('App\Kategori','kategori_id');
   	}

}
