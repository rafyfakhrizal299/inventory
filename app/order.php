<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class order extends Model
{
    protected $fillable = ['barang_id','id_pembuat','nama_pembeli','alamat','no_telepon','tanggal_beli','quantity','harga','jumlah','total_jumlah'];
   	public $timestamps = true;
   	//function tamu
   	public function Tagihan(){
   		return $this->hasMany('App\Tagihan','order_id');
   	}
   	public function Barang(){
   		return $this->belongsTo('App\Barang','barang_id');
   	}
   	public function User() {
		return $this->belongsTo('App\User', 'id_pembuat');
	}

	public function Order_detail() {
		return $this->hasMany('App\order_detail', 'id');
	}
}
