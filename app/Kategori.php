<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
   	protected $fillable = ['nama_kategori'];
   	public $timestamps = true;
   	//
   	public function Barang()
   	{
   		return $this->hasOne('App\Barang','kategori_id');
   	}
}
