<?php

namespace App;

use Laratrust\Models\LaratrustRole;

class Role extends LaratrustRole
{
    protected $fillable = ['name','display_name'];
   	public $timestamps = true;
   	//function tamu
	public function User() {
		return $this->hasMany('App\User', 'id');
	}
}
