<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Barang;
use App\Satuan;
use App\barang_masuk;
use App\Merk;
use App\Kategori;
use App\Supplier;
use Excel;
use PDF;
use Yajra\DataTables\Html\Builder;
use Yajra\DataTables\DataTables;


class BarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function jsonbarang(){
        $barang = Barang::all(); 
        return Datatables::of($barang)
        ->addColumn('harga_beli', function($barang){
            return 'Rp.'.number_format($barang->harga_beli,'2',',','.');
        })
        ->addColumn('harga_jual', function($barang){
            return 'Rp.'.number_format($barang->harga_jual,'2',',','.');
        })
        ->addColumn('stok', function($barang){
            return $barang->stok.' '.$barang->Satuan->nama_satuan;
        })
        ->addColumn('merk_id', function($barang){
            return $barang->Merk->nama_merk;
        })
        ->addColumn('kategori_id', function($barang){
            return $barang->Kategori->nama_kategori;
        })
        ->addColumn('supplier_id', function($barang){
            return $barang->Supplier->nama_supplier;
        })
        ->addColumn('action', function($barang){
            return '<a href="#" class="btn btn-xs btn-primary editBarang" data-id="'.$barang->id.'">
            <i class="glyphicon glyphicon-edit"></i> Ubah</a>&nbsp;
            <a href="#" class="btn btn-xs btn-danger deleteBarang" id="'.$barang->id.'">
            <i class="glyphicon glyphicon-remove"></i> Hapus</a>';
            })
        ->rawColumns(['action'])->make(true);
    }   
    public function index()
    {
        $satuan = Satuan::all();
        $merk = Merk::all();
        $kategori = Kategori::all();
        $supplier = Supplier::all();
        return view('barang.index', compact('satuan','merk','kategori','supplier'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nama_barang' => 'required|',
            'harga_beli' => 'required|numeric',
            'harga_jual' => 'required|numeric',
            'stok' => 'required|numeric',
            'satuan_id'=>'required',
            'supplier_id'=>'required',
            'kategori_id'=>'required',
            
        ],[
            'nama_barang.required'=>':Attribute harus diisi',
            'harga_beli.required'=>':Attribute harus diisi',
            'harga_jual.required'=>':Attribute harus diisi',
            'stok.required'=>':Attribute harus diisi',
            'stok.numeric'=>':Attribute harus berupa angka',
            'satuan_id.required'=>'Attribute harus diisi',
            'supplier_id.required'=>'Attribute harus diisi',
            'kategori_id.required'=>'Attribute harus diisi'
        ]);
        $barang = new Barang;
        $barang->nama_barang = $request->nama_barang;
        $barang->jenis_asal = $request->jenis_asal;
        $barang->harga_beli =  str_replace(".","" ,$request->harga_beli);
        $barang->harga_jual = str_replace(".","",$request->harga_jual);
        $barang->stok = $request->stok;
        $barang->satuan_id = $request->satuan_id;
        $barang->merk_id = $request->merk_id;
        $barang->kategori_id = $request->kategori_id;
        $barang->supplier_id = $request->supplier_id;
        $barang->save(); 
        return response()->json(['success'=>true]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $barang = Barang::find($id);
        return $barang;

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $barang = Barang::findOrFail($id);
        $barang->nama_barang = $request->nama_barang;
        $barang->jenis_asal = $request->jenis_asal;
        $barang->harga_beli =  str_replace(".","" ,$request->harga_beli);
        $barang->harga_jual = str_replace(".","",$request->harga_jual);
        $barang->stok = $request->stok;
        $barang->satuan_id = $request->satuan_id;
        $barang->merk_id = $request->merk_id;
        $barang->kategori_id = $request->kategori_id;
        $barang->supplier_id = $request->supplier_id;
        $barang->save();
        return response()->json(['success'=>true]);
    }
    public function removedata(Request $request)
    {
        $barang = Barang::find($request->input('id'));
        if($barang->delete())
        {
            echo 'Data Deleted';
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function downloadExcel()
    {
        $barang = Barang::all();
        return Excel::create('Daftar Barang',function($excel) use ($barang){
            $excel->setTitle('Daftar Barang')
            ->setCreator('Admin');
            $excel->sheet('Daftar Barang', function($sheet) use ($barang){
                $row = 1;
                $sheet->row($row,[
                    'Nama Barang',
                    'Jenis/Ukuran Barang',
                    'Asal',
                    'Harga Beli',
                    'Harga Jual',
                    'Stok',
                    'Satuan',
                    'Merk',
                    'Kategori',
                    'Supplier',
                ]);
                foreach ($barang as $barangs) {
                    $sheet->row(++$row,[
                        $barangs->nama_barang,
                        $barangs->ukuran,
                        $barangs->asal,
                        $barangs->harga_beli,
                        $barangs->harga_jual,
                        $barangs->stok,
                        $barangs->Satuan->nama_satuan,
                        $barangs->Merk->nama_merk,
                        $barangs->Kategori->nama_kategori,
                        $barangs->Supplier->nama_supplier,

                    ]);
                }
            });
        })->export('xls');
    }
    public function downloadPDF(Request $request)
    {
         $barang = Barang::all();
         
        if($request->view_type === 'download') {
            $pdf = PDF::loadView('barang.pdf', ['barang' => $barang]);
            return $pdf->download('List Barang.pdf');
        } else {
            $view = View('barang.pdf', ['barang' => $barang]);
            $pdf = \App::make('dompdf.wrapper');
            $pdf->loadHTML($view->render());
            return $pdf->stream();
        }
    }
}
