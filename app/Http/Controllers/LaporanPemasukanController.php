<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\order_detail;
use App\order;
use App\tagihan;
use Carbon\Carbon;
use App\Barang;
class LaporanPemasukanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tagihan = tagihan::whereDate('created_at', Carbon::today())->get();
        $order_detail =  order_detail::whereDate('created_at', Carbon::today())->get();
        $order = order::whereDate('created_at', Carbon::today())->get();
        $satuan_coba =  Barang::join('barang_keluars', 'barangs.id', '=' , 'barang_keluars.barang_id')
                          ->whereDate('barang_keluars.created_at', Carbon::today())
                          ->where('barangs.satuan_id','1')
                          ->get();
        $satuan_coba2 =  Barang::join('barang_keluars', 'barangs.id', '=' , 'barang_keluars.barang_id')
                          ->whereDate('barang_keluars.created_at', Carbon::today())
                          ->where('barangs.satuan_id','2')
                          ->get();
        $belum_bayar =  tagihan::whereDate('tagihans.created_at', Carbon::today())
                          ->where('tagihans.status','0')
                          ->get();
        $belum_lunas =  tagihan::whereDate('tagihans.created_at', Carbon::today())
                          ->where('tagihans.status','1')
                          ->get();
        $sudah_lunas =  tagihan::whereDate('tagihans.created_at', Carbon::today())
                          ->where('tagihans.status','2')
                          ->get();    
        return view('laporan_pemasukan/index',[
            'order' => $order,
            'order_detail' => $order_detail,
            'tagihan' => $tagihan,
            'satuan_coba' => $satuan_coba,
            'satuan_coba2' => $satuan_coba2,
            'belum_bayar' => $belum_bayar,
            'belum_lunas' => $belum_lunas,
            'sudah_lunas' => $sudah_lunas
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function index2(Request $request)
    {
        $dari = $request->dari;
        $sampai = $request->sampai;
        $order_detail = order_detail::whereBetween('created_at', [$dari, $sampai])->get();
        $tagihan =  tagihan::whereBetween('created_at', [$dari, $sampai])->get();
        $order = order::whereBetween('created_at', [$dari, $sampai])->get();
        $satuan_coba =  Barang::join('barang_keluars', 'barangs.id', '=' , 'barang_keluars.barang_id')
                          ->whereBetween('barang_keluars.created_at', [$dari, $sampai])
                          ->where('barangs.satuan_id','1')
                          ->get();
        $satuan_coba2 =  Barang::join('barang_keluars', 'barangs.id', '=' , 'barang_keluars.barang_id')
                          ->whereBetween('barang_keluars.created_at', [$dari, $sampai])
                          ->where('barangs.satuan_id','2')
                          ->get();
        $belum_bayar =  tagihan::whereBetween('tagihans.created_at', [$dari, $sampai])
                          ->where('tagihans.status','0')
                          ->get();
        $belum_lunas =  tagihan::whereBetween('tagihans.created_at', [$dari, $sampai])
                          ->where('tagihans.status','1')
                          ->get();
        $sudah_lunas =  tagihan::whereBetween('tagihans.created_at', [$dari, $sampai])
                          ->where('tagihans.status','2')
                          ->get();    
        return view('laporan_pemasukan/index2',[
            'order' => $order,
            'order_detail' => $order_detail,
            'satuan_coba' => $satuan_coba,
            'satuan_coba2' => $satuan_coba2,
            'belum_bayar' => $belum_bayar,
            'belum_lunas' => $belum_lunas,
            'sudah_lunas' => $sudah_lunas,
            'dari' => $dari,
            'sampai' => $sampai
        ]);
    }
}
