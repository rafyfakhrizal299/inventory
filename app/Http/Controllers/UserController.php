<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;
use Hash;
use App\Merk;
use Yajra\DataTables\Html\Builder;
use Yajra\DataTables\DataTables;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function jsonuser(){
        $user = User::all();
        return Datatables::of($user)
        ->addColumn('action', function($user){
            return '<a href="#" class="btn btn-xs btn-primary editUser" data-id="'.$user->id.'">
            <i class="glyphicon glyphicon-edit"></i> Edit</a>&nbsp;
            <a href="#" class="btn btn-xs btn-danger deleteUser" id="'.$user->id.'">
            <i class="glyphicon glyphicon-remove"></i> Delete</a>';
            })
        ->rawColumns(['action'])->make(true);
    }
    public function index()
    {
        return view('user/index',compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => bcrypt($request['password']),
        ]);
        
        $Role = Role::Where('name', 'karyawan')->first();
        $user->attachRole($Role);
        $user->save();
        return response()->json(['success'=>true]);    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function removedata(Request $request)
    {
        $merk = Merk::find($request->input('id'));
        if($merk->delete())
        {
            echo 'Data Deleted';
        }
    }
}
