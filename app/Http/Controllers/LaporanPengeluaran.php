<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\barang_masuk;
use App\Barang;
use Carbon\Carbon;

class LaporanPengeluaran extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $barang_masuk = barang_masuk::whereDate('created_at', Carbon::today())->get();
        $satuan_coba =  Barang::join('barang_masuks', 'barangs.id', '=' , 'barang_masuks.barang_id')
                          ->whereDate('barang_masuks.created_at', Carbon::today())
                          ->where('barangs.satuan_id','1')
                          ->get();
        $satuan_coba2 =  Barang::join('barang_masuks', 'barangs.id', '=' , 'barang_masuks.barang_id')
                          ->whereDate('barang_masuks.created_at', Carbon::today())
                          ->where('barangs.satuan_id','2')
                          ->get();
        return view('laporan_pengeluaran/index',[
            'barang_masuk' => $barang_masuk,
            'satuan_coba' => $satuan_coba,
            'satuan_coba2' => $satuan_coba2
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function index2(Request $request)
    {
        $dari = $request->dari;
        $sampai = $request->sampai;
        $barang_masuk = barang_masuk::whereBetween('created_at', [$dari, $sampai])->get();
        $satuan_coba =  Barang::join('barang_masuks', 'barangs.id', '=' , 'barang_masuks.barang_id')
                          ->whereBetween('barang_masuks.created_at', [$dari, $sampai])
                          ->where('barangs.satuan_id','1')
                          ->get();
        $satuan_coba2 =  Barang::join('barang_masuks', 'barangs.id', '=' , 'barang_masuks.barang_id')
                          ->whereBetween('barang_masuks.created_at', [$dari, $sampai])
                          ->where('barangs.satuan_id','2')
                          ->get();
        return view('laporan_pengeluaran.index2', compact('barang_masuk','satuan_coba','satuan_coba2', 'dari','sampai'));    
    }
}
