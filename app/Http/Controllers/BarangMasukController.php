<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Barang;
use App\Supplier;
use App\barang_masuk;
use Yajra\DataTables\Html\Builder;
use Yajra\DataTables\DataTables;

class BarangMasukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function jsonbarangmasuk(){
        $barang_masuk = barang_masuk::all(); 
        return Datatables::of($barang_masuk)
        ->addColumn('harga_beli', function($barang){
            return 'Rp'.number_format($barang->harga_beli,'2',',','.');
        })
        ->addColumn('tanggal_masuk', function ($barang_masuk) {
              return date('d F Y' , strtotime($barang_masuk->created_at));
            })
        ->addColumn('stok_masuk', function ($barang_masuk) {
              return $barang_masuk->stok_masuk.' '.$barang_masuk->Barang->Satuan->nama_satuan;
            })
        ->addColumn('barang_id', function($barang_masuk){
            return $barang_masuk->Barang->nama_barang;
        })
        ->addColumn('id_supplier', function($barang_masuk){
            return $barang_masuk->Supplier->nama_supplier;
        })
        ->addColumn('total', function($barang){
            return 'Rp'.number_format($barang->total,'2',',','.');
        })
        ->addColumn('action', function($barang_masuk){
            return '<a href="#" class="btn btn-xs btn-primary editBarangMasuk" data-id="'.$barang_masuk->id.'">
            <i class="glyphicon glyphicon-edit"></i> Ubah</a>&nbsp;
            <a href="#" class="btn btn-xs btn-danger deleteBarangMasuk" id="'.$barang_masuk->id.'">
            <i class="glyphicon glyphicon-remove"></i> Hapus</a>';
            })
        ->rawColumns(['action'])->make(true);
    }
    public function index()
    {       
        $barang = Barang::all();
        $supplier = Supplier::all();
        return view('barang_masuk.index',compact('barang','supplier'));
    }
    public function findSupplier(Request $request){
        // $data=barang_masuk::where('id_supplier',$request->id)->take(100)->get();
        // return response()->json($data);
        $supplier = Supplier::find($request->id);
        $id_supplier = $supplier->id;
        $nama_supplier = $supplier->nama_supplier;
        $nama_perusahaan = $supplier->nama_perusahaan;
        $no_telp = $supplier->no_telp;
        $alamat = $supplier->alamat;
          return response()->json([
            "id_supplier" => $id_supplier,
            "no_telp" => $no_telp,
            "nama_supplier" => $nama_supplier,
            "nama_perusahaan"=> $nama_perusahaan,
            "alamat" => $alamat,
          ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            for ($id=0; $id < count($request->barang_id); $id++) { 
            $barang_masuk = new barang_masuk ;
            $barang_masuk->barang_id = $request->barang_id[$id];
            $barang_masuk->stok_masuk = $request->stok_masuk[$id];
            $barang_masuk->harga_beli = $request->harga_beli[$id];
            $barang_masuk->id_pembuat = $request->id_pembuat;
            $barang_masuk->id_supplier = $request->id_supplier;
            $barang_masuk->total = $request->stok_masuk[$id] * $request->harga_beli[$id];
            //menambah stock
            $barang = Barang::findOrFail($request->barang_id[$id]);
            $barang->stok = $barang->stok + $request->stok_masuk[$id];
            $barang->harga_beli = $request->harga_beli[$id];
            $barang->save();
            $barang_masuk->save();
        }
        return response()->json(['success'=>true]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $barang_masuk = barang_masuk::find($id);
        return $barang_masuk;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $barang_masuk = barang_masuk::find($id);
        $barang_masuk->barang_id          = $request->barang_id;
        $barang_masuk->harga_beli         = $request->harga_beli;  
        $barang_masuk->stok_masuk          = $request->stok_masuk;
        $barang_masuk->id_supplier    = $request->id_supplier;
        $barang_masuk->id_pembuat    =  $request->id_pembuat;
        $barang_masuk->total = $request->stok_masuk * $request->harga_beli;
        $barang_masuk->save();

        $barang = Barang::findOrFail($request->barang_id);
        $barang->stok = $barang->stok + $request->stok_masuk;
        $barang->harga_beli = $request->harga_beli;
        $barang->save();
        return redirect()->route('barang_masuk.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function removedata(Request $request)
    {
        $barang_masuk = barang_masuk::find($request->input('id'));
        if($barang_masuk->delete())
        {
            echo 'Data Deleted';
        }
    }
}
