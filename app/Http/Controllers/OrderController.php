<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Barang;
use App\Satuan;
use App\Supplier;
use App\Kategori;
use App\Merk;
use App\order;
use App\barang_keluar;
use Session;
use App\order_detail;
use App\tagihan;
use App\User;
use Yajra\DataTables\Html\Builder;
use Yajra\DataTables\DataTables;
use App\tb_s_toko;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function __construct()
    {
        $this->middleware('auth');
    }
    public function jsonorder(){
        $order_detail = order_detail::groupBy('kode_order')->get(); 
        return Datatables::of($order_detail)
        ->addColumn('select', function($order_detail){
            return '<center><input type="checkbox" name="checkitem[]" class="checkitem" value="'.$order_detail->kode_order.'"></center>';
            })
        ->addColumn('id_order', function($order_detail){
            return $order_detail->Order->nama_pembeli;
        })
        ->addColumn('id_order2', function($order_detail){
            return $order_detail->Order->tanggal_beli;
        })
        ->addColumn('action', function($order_detail){
            return '<a href="#" class="btn btn-xs btn-warning showData" id="'.$order_detail->id.'"><i class="fa fa-eye"></i> Lihat Data</a>
            <a href="#" class="btn btn-xs btn-primary editOrder" id="'.$order_detail->id.'"><i class="fa fa-edit"></i> Ubah</a>';
            })
        ->rawColumns(['action','select'])->make(true);
    }
    public function index()
    {
        $order=order::all();
        $supplier=Supplier::all();
        $barang=Barang::all();
        $user=User::all();
        // $order_detail=order_detail::orderBy('kode_order','desc')->first();
        // $kode_order=order_detail::where('kode_order')->count();
        return view('order1.index', compact('barang','user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    public function findPrice(Request $request)
    {
        $data=Barang::select('harga_jual','stok','nama_barang','satuan_id')->where('id',$request->id)->first();
        return response()->json($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nama_pembeli' => 'required|',
            'alamat' => 'required',
            'no_telepon' => 'required|numeric',
            'uang_muka'=>'required|numeric',
            
        ],[
            'nama_pembeli.required'=>':Attribute harus diisi',
            'alamat.required'=>':Attribute harus diisi',
            'no_telepon.required'=>':Attribute harus diisi',
            'no_telepon.numeric'=>':Attribute harus berupa angka',
            'uang_muka.required'=>':Attribute harus diisi',
            'uang_muka.numeric'=>':Attribute harus berupa angka',
        ]);
        for($id = 0; $id < count($request->barang_id); $id++){
        $order=new order;

        $order->nama_pembeli=$request->nama_pembeli;
        $order->alamat=$request->alamat;
        $order->no_telepon=$request->no_telepon;
        $order->tanggal_beli=$request->tanggal_beli;
        $order->harga=$request->harga_jual[$id];
        $order->quantity=$request->kuantitas[$id];
        $order->jumlah=$request->kuantitas[$id] * $request->harga_jual[$id];
        $order->barang_id=$request->barang_id[$id];
        $order->id_pembuat=$request->id_pembuat;
        $order->save();
        
        $barang = Barang::find($request->barang_id[$id]);
        $barang->stok = $barang->stok - $request->kuantitas[$id];
        $barang->save();

        $order_detail = new order_detail;
        $order_detail->order_id=$order->id;
        $order_detail->kode_order=$request->kode_order;
        $order_detail->sub_total=str_replace(".","" ,$request->subtotal);
        $order_detail->potongan_harga=$request->potongan_harga;
        $order_detail->total= str_replace(".","" ,$request->grand_total);
        $order_detail->uang_muka=$request->uang_muka;
        $order_detail->metode_pengiriman=$request->metode_pengiriman;
        $order_detail->save();
        
        $tagihan = new tagihan;
        $tagihan->order_detail_id=$order_detail->id;
        $tagihan->total_pembayaran=$request->uang_muka;
        $tagihan->sisa_pembayaran=$order_detail->total - $request->uang_muka;
        $tagihan->jumlah_pembayaran=$order_detail->total;
        // if ($request->uang_muka >$request->sub_total2) {
        //     $tb_tagihan->uang_muka=$request->uang_muka - $request->sub_total2
            
        // }
        // if ($request->uang_muka = 0) {
        //     $tagihan->status = '1';
        // }
        if ($request->uang_muka < 1  ) {
            $tagihan->status = '0';
        }
        elseif ($order_detail->total > $request->uang_muka) {
            $tagihan->status = '1';
        }
        elseif ($order_detail->total = $request->uang_muka){
            $tagihan->status = '2';
        }
        $tagihan->save();
        
        }
        return response()->json(['success'=>true]); 
 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $data_order = order_detail::find($id);
        // foreach ($data as $coba) {
        $order_detail = order_detail::where('kode_order',$data_order->kode_order)->with('order.barang.satuan')->get();
        return response()->json($order_detail);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $data_order = order_detail::find($id);
        // foreach ($data as $coba) {
        $order_detail = order_detail::where('kode_order',$data_order->kode_order)->with('order.barang.satuan')->get();
        return response()->json($order_detail);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function kode_order()
    {   
        $data=order_detail::orderBy('kode_order','desc')->first();
        
        return response()->json($data);
    }
    public function deleteDataSebagian(Request $request)
    {
       
        $data = $request->all();
        foreach($data["check"] as $check)
        {
            $delete = order_detail::where('kode_order', $check)->delete();
        }
        return response()->json(['success'=>true]);
    }
}
