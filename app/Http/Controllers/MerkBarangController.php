<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Merk;
use Yajra\DataTables\Html\Builder;
use Yajra\DataTables\DataTables;

class MerkBarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function __construct()
    {
        $this->middleware('auth');
    }
    public function jsonmerk(){
        $merk = Merk::all(); 
        return Datatables::of($merk)
        ->addColumn('action', function($merk){
            return '<a href="#" class="btn btn-xs btn-primary editMerk" data-id="'.$merk->id.'">
            <i class="glyphicon glyphicon-edit"></i> Ubah</a>&nbsp;
            <a href="#" class="btn btn-xs btn-danger deleteMerk" id="'.$merk->id.'">
            <i class="glyphicon glyphicon-remove"></i> Hapus</a>';
            })
        ->rawColumns(['action'])->make(true);
    }
    public function index()
    {
        return view('merk.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nama_merk' => 'required|',
            
        ],[
            'nama_merk.required'=>':Attribute harus diisi',
        ]);
        $merk = new Merk;
        $merk->nama_merk = $request->nama_merk;
        $merk->save();
        return response()->json(['success'=>true]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $merk = Merk::find($id);
        return $merk;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nama_merk' => 'required|',
            
        ],[
            'nama_merk.required'=>':Attribute harus diisi',
        ]);
        $merk = Merk::find($id);
        $merk->nama_merk = $request->nama_merk;
        $merk->save();
        return response()->json(['success'=>true]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function removedata(Request $request)
    {
        $merk = Merk::find($request->input('id'));
        if($merk->delete())
        {
            echo 'Data Deleted';
        }
    }
}