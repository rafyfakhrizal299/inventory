<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Supplier;
use Excel;
use PDF;
use Yajra\DataTables\Html\Builder;
use Yajra\DataTables\DataTables;

class SupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
     public function jsonsupplier(){
        $supplier = Supplier::all(); 
        return Datatables::of($supplier)
        ->addColumn('action', function($supplier){
            return '<a href="#" class="btn btn-xs btn-primary editSupplier" data-id="'.$supplier->id.'">
            <i class="glyphicon glyphicon-edit"></i> Ubah</a>&nbsp;
            <a href="#" class="btn btn-xs btn-danger deleteSupplier" id="'.$supplier->id.'">
            <i class="glyphicon glyphicon-remove"></i> Hapus</a>';
            })
        ->rawColumns(['action'])->make(true);
    }
    public function index()
    {
        return view('supplier.index', compact('supplier'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nama_supplier' => 'required|',
            'nama_perusahaan' => 'required',
            'alamat'=>'required',
            'no_telp'=>'required|numeric'
            
        ],[
            'nama_supplier.required'=>':Attribute harus diisi',
            'nama_perusahaan.required'=>':Attribute harus diisi',
            'alamat.required'=>':Attribute harus diisi',
            'no_telp.required'=>':Attribute harus diisi',
            'no_telp.numeric'=>':Attribute harus berupa angka',
        ]);
        $supplier = new Supplier;
        $supplier->nama_supplier = $request->nama_supplier;
        $supplier->nama_perusahaan = $request->nama_perusahaan;
        $supplier->alamat = $request->alamat;
        $supplier->no_telp = $request->no_telp;
        $supplier->save();
        return response()->json(['success'=>true]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $supplier = Supplier::find($id);
        return $supplier;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $supplier = Supplier::findOrFail($id);
        $supplier->nama_supplier = $request->nama_supplier;
        $supplier->nama_perusahaan = $request->nama_perusahaan;
        $supplier->alamat = $request->alamat;
        $supplier->no_telp = $request->no_telp;
        $supplier->save();
        return response()->json(['success'=>true]);
    }
    public function removedata(Request $request)
    {
        $supplier = Supplier::find($request->input('id'));
        if($supplier->delete())
        {
            echo 'Data Deleted';
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $supplier =Supplier::destroy($id);
        return redirect()->back();
    }
    public function downloadExcel()
    {
        $supplier = Supplier::all();
        return Excel::create('Daftar Supplier',function($excel) use ($supplier){
            $excel->setTitle('Daftar Supplier')
            ->setCreator('Admin');
            $excel->sheet('Daftar Supplier', function($sheet) use ($supplier){
                $row = 1;
                $sheet->row($row,[
                    'Nama Supplier',
                    'Nama Perusahaan',
                    'Alamat',
                    'No Telepon',
                ]);
                foreach ($supplier as $sup) {
                    $sheet->row(++$row,[
                        $sup->nama_supplier,
                        $sup->nama_perusahaan,
                        $sup->alamat,
                        $sup->no_telp,
                    ]);
                }
            });
        })->export('xls');
    }
    public function downloadPDF(Request $request)
    {
         $sup = Supplier::all();
         
        if($request->view_type === 'download') {
            $pdf = PDF::loadView('supplier.pdf', ['sup' => $sup]);
            return $pdf->download('List supplier.pdf');
        } else {
            $view = View('supplier.pdf', ['sup' => $sup]);
            $pdf = \App::make('dompdf.wrapper');
            $pdf->loadHTML($view->render());
            return $pdf->stream();
        }
    }
}
