<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\order_detail;
use Yajra\DataTables\Html\Builder;
use Yajra\DataTables\DataTables;

class DetailOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function jsonorderdetail(){
        $order_detail = order_detail::groupBy('kode_order')->get(); 
        return Datatables::of($order_detail)
        ->addColumn('id_order', function($order_detail){
            return $order_detail->Order->nama_pembeli;
        })
        ->addColumn('id_order2', function($order_detail){
            return $order_detail->Order->tanggal_beli;
        })
        ->addColumn('action', function($order_detail){
            return '<a href='.action('DetailOrderController@printInvoice', ['kode_order' => $order_detail->kode_order]).' class="btn btn-xs btn-success ">
            <i class="glyphicon glyphicon-print"></i> Print</a>&nbsp;
            <a href="#" class="btn btn-xs btn-danger showData" id="'.$order_detail->kode_order.'"><i class="glyphicon glyphicon-remove"></i> Show Data</a>';
            })
        ->rawColumns(['action'])->make(true);
    }
    public function index()
    {
        $order_detail=order_detail::groupBy('kode_order');

        return view('order_detail.index',compact('order_detail'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function printInvoice(Request $request)
    {
        $order_detail = order_detail::where('kode_order',$request->kode_order)->get();
        // dump($order_detail);
        return view ('print.nota',compact('order_detail'));
    }
    public function deleteDataSebagian(Request $request)
    {
       
        $data = $request->all();
        foreach($data["check"] as $check)
        {
            $delete = order_detail::where('kode_order', $check)->delete();
        }
        return response()->json(['success'=>true]);
    }
}
