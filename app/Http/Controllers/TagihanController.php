<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\tagihan;
use App\Barang;
use Yajra\DataTables\Html\Builder;
use Yajra\DataTables\DataTables;

class TagihanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function jsontagihan(){
        $tagihan = tagihan::all(); 
        return Datatables::of($tagihan)
        ->addColumn('id_order_detail1', function($tagihan){
            return $tagihan->order_detail->kode_order;
        
        })
        ->addColumn('id_order_detail2', function($tagihan){
            return $tagihan->order_detail->Order->nama_pembeli;
        })
        ->addColumn('status',function($tagihan){
            if ($tagihan->status == 2) {
            return 'Sudah Lunas';   
            }
            elseif ($tagihan->status == 1) {
            return 'Belum Lunas';   
            }

            elseif ($tagihan->status == 0) {
            return 'Belum Bayar';   
            }
        })
        ->addColumn('action', function($tagihan){
            return '<a href="#" class="btn btn-xs btn-primary editTagihan" data-id="'.$tagihan->id.'">
            <i class="glyphicon glyphicon-edit"></i> Bayar</a>&nbsp;
            <a href="#" class="btn btn-xs btn-danger deleteTagihan" id="'.$tagihan->id.'">
            <i class="glyphicon glyphicon-remove"></i> Hapus</a>';
            })
        ->rawColumns(['action'])->make(true);
    }
    public function index()
    {
        $barang = Barang::all();
        return view('tagihan.index', compact('barang'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tagihan = tagihan::find($id);
        return $tagihan;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tagihan = tagihan::findOrFail($id);
        $tambah = $request->down_payment + $tagihan->total_pembayaran;
        $tagihan->total_pembayaran = $tambah;
        $tagihan->sisa_pembayaran = $tagihan->jumlah_pembayaran - $tambah;
        if ($request->down_payment < 1  ) {
            $tagihan->status = '0';
        }
        elseif ($order_detail->total > $request->down_payment) {
            $tagihan->status = '1';
        }
        elseif ($order_detail->total = $request->down_payment){
            $tagihan->status = '2';
        }
        $tagihan->save();
        return response()->json(['success'=>true]);
    }

    /**
     * Remove the specified resouif ($request->down_payment < $request->grand_total) {
            $tagihan->status = '1';
        }
        elseif ($request->down_payment = 0) {
            $tagihan->status = '0';
        }
        else {
            $tagihan->status = '2';
        }rce from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function removedata(Request $request)
    {
        $tagihan = tagihan::find($request->input('id'));
        if($tagihan->delete())
        {
            echo 'Data Deleted';
        }
    }
}
