<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Barang;
use App\barang_keluar;
use App\order;
use Excel;
use PDF;
use Yajra\DataTables\Html\Builder;
use Yajra\DataTables\DataTables;

class BarangKeluarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function jsonbarangkeluar(){
        $order = order::all(); 
        return Datatables::of($order)
        ->addColumn('barang_id', function($order){
            return $order->Barang->nama_barang;
        })
        ->addColumn('id_pembuat', function($order){
            return $order->User->name;
        })  
        ->addColumn('action', function($order){
            return '<a href="#" class="btn btn-xs btn-primary editBarangKeluar" data-id="'.$order->id.'">
            <i class="glyphicon glyphicon-edit"></i> Ubah</a>&nbsp;
            <a href="#" class="btn btn-xs btn-danger deleteBarangKeluar" id="'.$order->id.'">
            <i class="glyphicon glyphicon-remove"></i> Hapus</a>';
            })
        ->rawColumns(['action'])->make(true);
    }
    public function index()
    {       
        $barang = Barang::all();
        return view('barang_keluar.index',compact('barang'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $order = order::find($id);
        return $order;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $order = order::findOrFail($id);
        $order->quantity = $request->kuantitas;
        $order->jumlah = $request->harga * $request->kuantitas;
        $order->save();
        return response()->json(['success'=>true]);
    }
    public function removedata(Request $request)
    {
        $order = order::find($request->input('id'));
        if($order->delete())
        {
            echo 'Data Deleted';
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function downloadExcel()
    {
        $order = order::all();
        return Excel::create('Daftar order',function($excel) use ($order){
            $excel->setTitle('Daftar order')
            ->setCreator('Admin');
            $excel->sheet('Daftar order', function($sheet) use ($order){
                $row = 1;
                $sheet->row($row,[
                    'Nama Barang',
                    'Pembuat',
                    'Nama Pembeli',
                    'Tanggal Masuk',
                    'Kuantitas',
                    'Harga',
                    'Jumlah',
                ]);
                foreach ($order as $orders) {
                    $sheet->row(++$row,[
                        $orders->nama_barang,
                        $orders->User->name,
                        $orders->nama_pembeli,
                        $orders->tanggal_masuk,
                        $orders->quantity,
                        $orders->harga,
                        $orders->stok,
                        $orders->jumlah,

                    ]);
                }
            });
        })->export('xls');
    }
    public function downloadPDF(Request $request)
    {
         $order = order::all();
         
        if($request->view_type === 'download') {
            $pdf = PDF::loadView('barang_keluar.pdf', ['order' => $order]);
            return $pdf->download('List Barang Keluar.pdf');
        } else {
            $view = View('barang_keluar.pdf', ['order' => $order]);
            $pdf = \App::make('dompdf.wrapper');
            $pdf->loadHTML($view->render());
            return $pdf->stream();
        }
    }
}
