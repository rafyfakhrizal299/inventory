<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laratrust\LaratrustFacade as Laratrust;
use App\Barang;
use App\Supplier;
use App\barang_masuk;
use App\order_detail;
use App\order;
use Carbon\Carbon;
use App\tagihan;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $barang = Barang::all();
        $supplier = Supplier::all();
        $barang_masuk = barang_masuk::whereDate('created_at', Carbon::today())->get();
        $order_detail = order_detail::whereDate('created_at', Carbon::today())->get();
        $order = order::whereDate('created_at', Carbon::today())->get();
        $belum_bayar =  tagihan::whereDate('tagihans.created_at', Carbon::today())
                          ->where('tagihans.status','3')
                          ->get();
        $belum_lunas =  tagihan::whereDate('tagihans.created_at', Carbon::today())
                          ->where('tagihans.status','0')
                          ->get();
        $sudah_lunas =  tagihan::whereDate('tagihans.created_at', Carbon::today())
                          ->where('tagihans.status','1')
                          ->get();
        if (Laratrust::hasRole('Admin')){
            return view('dashboard.admin',[
                'barang' => $barang,
                'supplier'=> $supplier,
                'barang_masuk' => $barang_masuk,
                'order' => $order,
                'order_detail' => $order_detail,
                'belum_bayar' => $belum_bayar,
                'belum_lunas' => $belum_lunas,
                'sudah_lunas' => $sudah_lunas
            ]);
        }
        else if (Laratrust::hasRole('Karyawan')){
            return view('dashboard.karyawan',[
                'barang' => $barang,
                'supplier'=> $supplier,
                'barang_masuk' => $barang_masuk,
                'order' => $order,
                'order_detail' => $order_detail,
                'belum_bayar' => $belum_bayar,
                'belum_lunas' => $belum_lunas,
                'sudah_lunas' => $sudah_lunas
            ]);          
        }
    }
}
