<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Merk extends Model
{
    protected $fillable = ['nama_merk'];
    public $timestamp = true;
    //
    public function Barang(){
    	return $this->hasOne('App\Barang','merk_id');
    }
}
