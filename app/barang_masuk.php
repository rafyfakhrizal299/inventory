<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class barang_masuk extends Model
{
    protected $fillable = ['barang_id','stok_masuk','harga_beli','id_pembuat','id_supplier'];
    public $timestamps = true;
    //foreign
    public function Barang()
    {
    	return $this->belongsTo('App\Barang','barang_id');
    }
    public function User()
    {
    	return $this->belongsTo('App\User','id_pembuat');
    }
    public function Supplier()
    {
    	return $this->belongsTo('App\Supplier','id_supplier');
    }
}
