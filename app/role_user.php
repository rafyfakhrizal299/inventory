<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class role_user extends Model
{
    protected $table = 'role_user';
	protected $fillable = array('user_id','role_id');
	public $timestamp = true;

	public function Role() {
		return $this->belongsTo('App\Role', 'role_id');
	}
	public function User() {
		return $this->belongsTo('App\User', 'user_id');
	}
}
