<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class barang_keluar extends Model
{
    protected $fillable = ['barang_id','stok_keluar','harga_jual','id_pembuat'];
    public $timestamps = true;
    //foreign
    public function Barang()
    {
    	return $this->belongsTo('App\Barang','barang_id');
    }
    public function User()
    {
    	return $this->belongsTo('App\User','id_pembuat');
    }
}
