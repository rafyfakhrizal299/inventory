<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class order_detail extends Model
{
    protected $table = 'order_details';
	protected $fillable = array('order_id','kode_order');
	public $timestamp = true;

	public function Order() {
		return $this->belongsTo('App\order', 'order_id');
	}
}
