<input type='checkbox' name='checkitem[]' class='checkitem' value='$barang->kode_barang'>

public function deleteDataSebagian(Request $request)
    {
       
        $data = $request->all();
        foreach($data["check"] as $check)
        {
            $barang = barang::findOrFail($check);
            $barang->deleted_at = date("Y-m-d");
            // dd($barang->deleted_at);
            $barang->save();
        }
        $respone=[
            'status'=>'success',
            'message'=>'data telah dihapus'
        ];
        return json_encode($respone);
    }

<button  class="btn btn-danger box-shadow-2 px-2 button-barang" name="bulk_delete_barang" id="bulk_delete_barang" aria-haspopup="true" aria-expanded="false" ><i class="ft-trash"></i> Hapus Data</button>

Route::post('/barang/hapus-barang','barangController@deleteDataSebagian')->name('deleteBarang.beberapa');

deleteBulkBarang();
    
    function deleteBulkBarang() {
        $(document).on('click', '#bulk_delete_barang' ,function(){
			var $check =[];
            var $check = $(".checkitem:checked").map(function(){
                return $(this).val();
            }).get();
            console.log($check);
          if(confirm("Apakah anda yakin ingin menghapus data ini?"))
          {
              if($check.length > 0)
              {
                  $.ajax({
                      url:"/barang/hapus-barang",
                      method:"post",
                      data:{
                        _token:token,
                          check:$check
                          },
                      complete: function() {
                        swal("Berhasil!", "Data Anda Telah diHapus!", "success");
                        dataTableBarang.ajax.reload( null, false );
                    }
                  });
              }else

              {
                  alert("Pilih satu data")
              }
          }
        });
  
    }