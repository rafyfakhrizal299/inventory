@extends('layouts.admin')
@section('content')
<br />
            <div class="col-md-12 col-sm-12 col-xs-12">
              <ul class="breadcrumb">
                <li><a href="home"><i class="fa fa-home"></i></a></li>
                <li>Order</li>
              </ul>
            </div>
            
<div class="col-md-12 col-sm-12 col-xs-12 barangKeluar" id="indexOrder">
   <div id="success"></div>
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Data Order </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <button type="button" class="btn btn-success" id="TambahBarangKeluarChange">Tambah Data</button>
                    <button  class="btn btn-danger bulk_delete_barang" name="bulk_delete_barang" id="bulk_delete_barang" aria-haspopup="true" aria-expanded="false" ><i class="ft-trash"></i> Hapus Data</button>
                    <br>
                    <table class="table table-striped table-bordered responsive" id="dataTableOrder">
                        <thead>
                          <tr>
                            <th>Pilih</th>
                            <th>Kode Order</th>
                            <th>Nama Pembeli</th>
                            <th>Tanggal</th>  
                            <th>Aksi</th>
                        </tr>
                      </thead>
                    </table>
                  </div>
                </div>
              </div>
@include('order.create')
@include('order.modal')
@endsection
@section('js')
  <script>
  $(document).ready(function(){
    // $('.barangselect').change(function()
    //  {
    //   var bar_id=$(this).val();
    //   console.log(bar_id);
    //   var op="";
    //   $.ajax({
    //     type:'get',
    //     url:'{!!URL::to('findPrice')!!}',
    //     data:{'id':bar_id},
    //     dataType:'json',//return data will be json
    //     success:function(data){
    //       console.log("price");
    //       console.log(data.price);

    //       $("input[name='harga_jual']").val(data.harga_jual);

    //     },
    //     error:function(){

    //     }
    //   });
    // });
    $('.x_content').delegate('.barangselect','change',function(){
    var tr=$(this).parent().parent();
    var id =tr.find('.barangselect').val();
    var dataId={'id':id};
    console.log(dataId);
    $.ajax({
      type      :'GET',
      url       :'{!!URL::to('findprice')!!}',
      dataType  :'json',
      data      :dataId,

      success:function(data){
        if (data.stok > 0) {
          tr.find('.harga_jual').val(data.harga_jual);
          tr.find('.stok').val(data.stok);
          tr.find('.stok_sisa').val(data.stok);
        }
        else if(data.stok <= 0){
          $('#stokkosong').show();
          var stokkosong = "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button>"
          stokkosong += "Stok Barang <b>"+data.nama_barang+"</b> Kosong , Silahkan Tambahkan Stok Barang <a href='barangmasuk'>Klik disini</a>";
          $('#stokkosong').addClass('alert alert-danger alert-dismissible fade in').html(stokkosong);
          setTimeout(function(){
            $('#stokkosong').fadeOut('slow');
          }, 5000);
          tr.find('.stok_sisa').val(data.stok);
        }
      }
    });
  });
    $('.x_content').delegate('.kuantitas,.harga_jual,.stok_sisa,.stok,#potongan_harga', 'keyup , scroll',function(){
        var tr =$(this).parent().parent();
        var kuantitas =tr.find('.kuantitas').val();
        var harga_jual =tr.find('.harga_jual').val();
        var stok =tr.find('.stok').val();
        var sisa_stok = (stok - kuantitas);
        var jumlah =(kuantitas * harga_jual);
        if ( parseInt(kuantitas) > parseInt(stok)) {
          $('#stokmin').show();
          var stokmin = "<button type='button' id='closeStok' class='close' aria-label='Close'><span aria-hidden='true'>×</span></button>"
          stokmin += "Kuantitas Melebihi Stok, Otomatis Input Semua Stok";
          $('#stokmin').addClass('alert alert-danger alert-dismissible fade in').html(stokmin);
          setTimeout(function(){
            $('#stokmin').fadeOut('slow');
          }, 7000);
          tr.find('.kuantitas').val(stok);
        }
        else if(parseInt(kuantitas) < 0){
          $('#stokmink').show();
          var stokmink = "<button type='button' id='closeStokM' class='close' aria-label='Close'><span aria-hidden='true'>×</span></button>"
          stokmink += "Tidak Boleh Minus";
          $('#stokmink').addClass('alert alert-danger alert-dismissible fade in').html(stokmink);
          setTimeout(function(){
            $('#stokmink').fadeOut('slow');
          }, 7000);
          tr.find('.kuantitas').val(0);
        }       
        // else if(tr.find('.kuantitas').val() > tr.find('.stok').val()){
        // // else if(tr.find('.stok_sisa').val() < 0){
        //   var stokmin = "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button>"
        //   stokmin += "Kuantitas Melebihi Stok";
        //   $('#stokmink').addClass('alert alert-danger alert-dismissible fade in').html(stokmink);
        //   setTimeout(function(){
        //     $('#stokmink').fadeOut('slow');
        //   }, 5000);
        //   tr.find('.kuantitas').val(stok);
        // }
        else{
          tr.find('.stok_sisa').val(sisa_stok);
          tr.find('.jumlah').val(jumlah);
          total();
        }
        // else{
        //   tr.find('.stok_sisa').val(sisa_stok);
        // }    
      });
      
      var count = 1
    $('#add').click(function(){
      count = count + 1;
      var html = "<tr id='row"+count+"'>";
      // $('.select-kan').select2();
      html +='<td><select name="barang_id[]" class="form-control barangselect select-kan" id="barang_id"><option>--Pilih Barang--</option>@foreach($barang as $data)<option value="{{$data->id}}">{{$data->nama_barang}}-{{$data->Satuan->nama_satuan}}-{{$data->Supplier->nama_supplier}}</option>@endforeach</select></td>';
      html +='<td><input type="text" name="kuantitas[]" min="1" max="data.stok" id="kuantitas" class="form-control kuantitas"/></td>';
      html +='<td><input type="number" name="stok_sisa[]" id="stok_sisa" class="form-control stok_sisa" readonly="" /></td><input type="hidden" name="stok[]" id="stok" class="form-control stok" readonly="" />';
      html +='<td><input type="number" name="harga_jual[]" id="harga_jual" class="form-control harga_jual" readonly="" /></td>';
      html +='<td><input type="text" name="jumlah" id="jumlah" class="form-control jumlah" placeholder="Jumlah" readonly/></td>';
      html +='<td><button type="button" class="btn btn-danger btn-sm remove" name="remove" data-row="row'+ count +'"><i class="fa fa-minus-square"></i></button></td></tr>';
      //cc
      $('.x_content #coba').append(html);
    });
    $(document).on('click', '.remove', function(){
      var delete_row = $(this).data("row");
      $('#' + delete_row).remove();
      total();
     });
    // function remove(no){
    //   $('#row_'+no).remove();
    // }
    function total()
      {
        var total =0;
        $('.jumlah').each(function(i,e){
          var jumlah = $(this).val()-0;
          total +=jumlah;
          var potongan_harga = document.getElementById('potongan_harga').value;
          $('.totalpotong').val(total - potongan_harga);
        })
        $('.subtotal').html("Rp." + total.formatMoney(2,'.'));
        $('.subtotal').val(total.formatMoney(2,'.'));
      };
      function grandtotal()
      {
        var subtotal = $('.subtotal').value;
      };
      Number.prototype.formatMoney = function(decPlaces, thouSeparator) {
      var n = this,
          decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces,
          thouSeparator = thouSeparator == undefined ? "." : thouSeparator,
          sign = n < 0 ? "-" : "",
          i = parseInt(n = Math.abs(+n || 0).toFixed(decPlaces)) + "",
          j =(j = i.length) > 3 ? j % 3 : 0;
          return sign + (j ? i.substr(0, j) + thouSeparator : "")
          + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thouSeparator);
     };
  });
  $(document).on('click mouseover mouseout mouseleave','#TambahBarangKeluarChange,#aksiOrder',function(){
    var id =$('.kode_order').val();
    var idi = (parseInt(id) + 1)
    var dataId={'id':idi};
    $.ajax({
      type      :'GET',
      url       :'{!!URL::route('kode_order') !!}',
      dataType  :'json',
      data      :dataId,

      success:function(data){
        $('.kode_order').val(parseInt(data.kode_order) + 1);
        if (data.kode_order == null) {
        $('.kode_order').val('1');  
        }
        
      }
    }); 
  });
  // function remove(count){
  //     $('#row_'+count).remove();
  //   }
    
  </script>
@endsection