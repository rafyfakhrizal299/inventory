<div class="col-md-12 col-sm-12 col-xs-12" id="TambahKeluar">
                <div id="validation"></div>
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Tambah Barang Keluar </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><button type="button" class="form-control" id="TutupTambahKeluar">Batal</button></li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">     
                   <div class="" role="tabpanel" data-example-id="togglable-tabs">
                      <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Tambah</a>
                        </li>
                        <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false"><i class ="fa fa-info">  Satuan</i></a>
                        </li>
                      </ul>
                       <div id="myTabContent" class="tab-content">
                          <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">               
                      <form id="formOrder" method="post" enctype="multipart/form-data">
                        {{csrf_field()}} {{ method_field('POST') }}
                      <div class="col-md-12 col-sm-12 col-xs-12">
                       <div class="x_panel">
                        <div class="x_title">
                          <h2>Form Pembelian </h2>
                          <div class="clearfix"></div>
                        </div>
                     <div class="x_content">
                      <div class="col-md-4 col-sm-4 col-xs-12">
                      <label>Kode Order</label>
                       <input type="text" name="kode_order" value="1" class="form-control kode_order" id="kode_order" placeholder="kode order" readonly>  
                     <span class="help-block has-error nama_pembeli_error"></span>
                      </div>
                      <div class="col-md-4 col-sm-4 col-xs-12">
                      <label>Nama Pembeli</label>
                      <input type="text" name="nama_pembeli" id="nama_pembeli" class="form-control" 
                     placeholder="Nama Pembeli">
                      </div>
                      <div class="col-md-4 col-sm-4 col-xs-12">
                        <label>No Telepon</label>
                      <input type="text" name="no_telepon" id="no_telepon" class="form-control" 
                     placeholder="No Telepon">
                      </div>
                      <div class="col-md-4 col-sm-4 col-xs-12">
                        <label>Tanggal Beli</label>
                      <input type="text" name="tanggal_beli" id="tanggal_beli" class="form-control" 
                     placeholder="" value="{{ Carbon\carbon::now()->toDateString() }}" readonly="">
                     <span class="help-block has-error nama_pembeli_error"></span>
                      </div> 
                       <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="form-group">
                        <label>Metode Pengiriman</label>
                      <select name="metode_pengiriman" class="form-control select-pilih" id="metode_pengiriman">
                        <option value="dikirim">Dikirim</option>
                        <option value="dibawa sendiri">Dibawa Sendiri</option>
                      </select>
                      </div>
                    </div>

                    <div class="col-md-12 col-sm-12 col-xs-12">
                     <div class="form-group">
                     <label>Alamat</label>
                     <textarea name="alamat" id="alamat" class="form-control" placeholder="Masukan Alamat" ></textarea>
                     <span class="help-block has-error ukuran_error"></span>
                  </div>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                  <label>Masukan Barang</label>
                  <div id="stokkosong"></div>
                  <div id="stokmin"></div>
                  <div id="stokmink"></div>
                    <table class="table table-striped table-bordered responsive" id="table_hidden">
                        <thead>
                          <tr>
                            <th>Nama Barang</th>
                            <th>Kuantitas</th>
                            <th>Stok</th>
                            <th>Harga Jual</th>
                            <th>Total</th>
                            <th>
                                <input type="hidden" name="id_pembuat" value="{{ Auth::user()->id }}">
                                <button type="button" name="add" class="btn btn-success btn-sm add" id="add"><i class="fa fa-plus-square"></i></button></th>
                          </tr>
                      </thead>
                      <tbody id="coba">
                          <tr id="row1">
                            <td><select name="barang_id[]" class="form-control barangselect select-pilih" id="barang_id">
                              <option>--Pilih Barang--</option>@foreach($barang as $data)<option value="{{$data->id}}">{{$data->nama_barang}}-{{$data->stok}} {{$data->Satuan->nama_satuan}}-{{$data->Supplier->nama_supplier}}</option>@endforeach</select></td>
                            <td><input type="number" min="1" max="data.stok" name="kuantitas[]" id="kuantitas" class="form-control kuantitas"/></td>
                            <td>
                                <input type="number" name="stok[]" id="stok_sisa" class="form-control stok_sisa" readonly="" />
                                <input type="hidden" name="stok[]" id="stok" class="form-control stok" readonly="" /></td>
                            <td><input type="number" name="harga_jual[]" id="harga_jual" class="form-control harga_jual" readonly="" /></td>
                            <td><input type="text" name="jumlah" id="jumlah" class="form-control jumlah" placeholder="Jumlah" readonly/></td>
                            <td><button type="button" class="btn btn-danger btn-sm" onclick="remove('+ no +')"><i class="fa fa-minus-square"></i></button></td>
                          </tr>
                        </tbody>
                    </table>
                    <div id="editTable"></div>
                  </div>
                </div>
              </div>
            </div>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                       <div class="x_panel">
                        <div class="x_title">
                          <h2>Pembayaran </h2>
                          <div class="clearfix"></div>
                        </div>
                    <div class="x_content">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group">
                      <label>Sub Total</label>
                        <div class="input-group">
                            <span class="input-group-btn">
                              <button type="button" class="form-control">Rp. </button>
                            </span>
                            <input type="text" name="subtotal" style="float:right" class="form-control subtotal" placeholder="Sub Total" readonly>
                            <span class="input-group-btn">
                              <button type="button" class="form-control">.00</button>
                            </span>
                            <span class="help-block has-error total_error"></span>
                        </div>
                      </div>
                    </div>
                  </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                      <div class="form-group">
                        <label>Potongan Harga</label>
                          <div class="input-group">
                            <span class="input-group-btn">
                              <button type="button" class="form-control">Rp. </button>
                            </span>
                            <input type="text" name="potongan_harga" style="float:right" class="form-control " id="potongan_harga" value="0">
                            <span class="input-group-btn">
                              <button type="button" class="form-control">.00</button>
                            </span>
                            <span class="help-block has-error potongan_harga_error"></span>
                          </div>
                      </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label>Uang Muka</label>
                          <div class="input-group">
                            <span class="input-group-btn">
                              <button type="button" class="form-control">Rp. </button>
                            </span>
                            <input type="text" name="uang_muka" style="float:right" class="form-control uang_muka" placeholder="Uang Muka">
                            <span class="input-group-btn">
                              <button type="button" class="form-control">.00 </button>
                            </span>
                          </div>
                    </div>
                  </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                  <div class="form-group">
                      <label>Grand Total</label>
                        <div class="input-group">
                            <span class="input-group-btn">
                              <button type="button" class="form-control">Rp. </button>
                            </span>
                            <input type="text" name="grand_total" style="float:right" class="form-control totalpotong" placeholder="Total Pembayaran" readonly>
                            <span class="input-group-btn">
                              <button type="button" class="form-control">.00</button>
                            </span>
                            <span class="help-block has-error total_error"></span>
                        </div>
                      </div>
                    </div>
                  </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                    <button type="submit" class="btn btn-primary" name="submit" id="aksiOrder"><i class="fa fa-check"></i> Simpan</button>
                  </div>
                </div>
              </form>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
              Ini info satuan
          </div>
        </div>
            </div>
          </div>
        </div>
      </div>
                
