<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Modal</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
<div class="modal fade" id="myModalSupplier" role="dialog">
    <div class="modal-dialog">
<div class="modal-content">
  <form id="formSupplier" method="post" enctype="multipart/form-data">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Masukan Data Supplier</h4>
        </div>
        <div class="modal-body">
          {{csrf_field()}} {{ method_field('POST') }}
          <input type="hidden" name="id" id="id">
          <div class="form-group">
                     <label>Nama Supplier</label>
                     <input type="text" name="nama_supplier" id="nama_supplier" class="form-control" 
                     placeholder="Nama Perusahaan">
                     <span class="help-block has-error nama_supplier_Naha error"></span>
                  </div>
                  <div class="form-group">
                     <label>Nama Perusahaan</label>
                     <input type="text" name="nama_perusahaan" id="nama_perusahaan" class="form-control" 
                     placeholder="Nama Perusahaan">
                     <span class="help-block has-error nama_perusahaan_error"></span>
                  </div>
                  <div class="form-group">
                     <label>Alamat</label>
                     <input type="text" name="alamat" id="alamat" class="form-control" 
                     placeholder="Alamat">
                     <span class="help-block has-error alamat_error"></span>
                  </div>
                  <div class="form-group">
                     <label>No Telepon</label>
                     <input type="text" name="no_telp" id="no_telp" class="form-control" 
                     placeholder="No Telepon">
                     <span class="help-block has-error no_telp_error"></span>
                  </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary" name="submit" id="aksiSupplier">Simpan</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </form>
      </div>
    </div>
  </div>
</body>
</html> 