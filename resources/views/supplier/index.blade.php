@extends('layouts.admin')
@section('content')
<br />
            <div class="col-md-12 col-sm-12 col-xs-12">
              <ul class="breadcrumb">
                <li><a href="home"><i class="fa fa-home"></i></a></li>
                <li>Supplier</li>
              </ul>
            </div>
<div class="col-md-12 col-sm-12 col-xs-12" id="indexSupplier">
  <div id="success"></div>
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Data Supplier </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                  	<button type="button" class="btn btn-success" id="TambahSupplier"><i class="glyphicon glyphicon-plus"></i>Tambah Data</button>
                    <button type="button" class="btn btn-warning" id="Print" data-toggle="modal" data-target="#printSupplier"><i class="glyphicon glyphicon-download"></i> Unduh Data</button>
                    @include('supplier.modal')
                    @include('supplier.print')
                    <table class="table table-striped table-bordered" id="dataTableSupplier">
                        <thead>
	                        <tr>
	                        	<th>Nama Supplier</th>
                            <th>Nama Perusahaan</th>
                            <th>Alamat</th>
                            <th>No Telepon</th>
	                        	<th>Aksi</th>
	                    	</tr>
                    	</thead>
                    </table>
                  </div>
                </div>
              </div>
@endsection