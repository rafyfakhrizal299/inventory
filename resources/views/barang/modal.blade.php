<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Modal</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
<div class="modal fade" id="myModalBarang" role="dialog">
    <div class="modal-dialog">
<div class="modal-content">
  <form id="formBarang" method="post" enctype="multipart/form-data">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Masukan Data Barang</h4>
        </div>
        <div class="modal-body">
          {{csrf_field()}} {{ method_field('POST') }}
          <input type="hidden" name="id" id="id">
          <div class="form-group">
                     <label>Nama Barang (Ukuran)</label>
                     <input type="text" name="nama_barang" id="nama_barang" class="form-control" 
                     placeholder="Nama Barang">
                     <span class="help-block has-error nama_barang_error"></span>
                  </div>
                  <div class="form-group">
                     <label>Jenis/Asal</label>
                     <input type="text" name="jenis_asal" id="jenis_ukuran" class="form-control" 
                     placeholder="Jenis/Asal (Jika Ada)">
                     <span class="help-block has-error jenis_ukuran_error"></span>
                  </div>
                  <div class="form-group">
                     <label>Harga Beli</label>
                     <input type="text" name="harga_beli" id="harga_beli" class="form-control" 
                     placeholder="Harga Beli">
                     <input type="hidden" id="raw_harga_beli" name="harga_beli" value="">
                     <span class="help-block has-error harga_beli_error"></span>
                  </div>
                  <div class="form-group">
                     <label>Harga Jual</label>
                     <input type="text" onkeypress="return number(event)" name="harga_jual" id="harga_jual" class="form-control" 
                     placeholder="Harga Jual">
                     <input type="hidden" id="raw_harga_jual" name="harga_jual" value="">
                     <span class="help-block has-error harga_jual_error"></span>
                  </div>
                  <div class="form-group">
                     <label>Stok</label>
                     <input type="text" name="stok" id="stok" class="form-control" 
                     placeholder="Stok">
                     <span class="help-block has-error nama_kategori_error"></span>
                  </div>
                  <div class="form-group {{ $errors->has('satuan_id') ? 'has-error' : '' }}">
                     <label>Satuan</label>
                     <select class="form-control select-pilih" name="satuan_id" id="satuan_id" style="width: 100%">
                        <option disabled selected>Pilih Satuan</option>
                        @foreach($satuan as $data)
                        <option value="{{$data->id}}">{{$data->nama_satuan}}</option>
                        @endforeach
                     </select>
                     @if ($errors->has('satuan_id'))
                     <span class="help-block has-error satuan_id_error">
                        <strong>{{$errors->first('satuan_id')}}</strong>
                     </span>
                     @endif
                  </div>
                  <div class="form-group {{ $errors->has('merk_id') ? 'has-error' : '' }}">
                     <label>Merk</label>
                     <select class="form-control select-pilih" data-live-search="true" name="merk_id" id="merk_id" style="width: 100%">
                        <option disabled selected>Pilih Merk</option>
                        @foreach($merk as $data)
                        <option value="{{$data->id}}">{{$data->nama_merk}}</option>
                        @endforeach
                     </select>
                     @if ($errors->has('satuan_id'))
                     <span class="help-block has-error merk_id_error">
                        <strong>{{$errors->first('merk_id')}}</strong>
                     </span>
                     @endif
                  </div>
                  <div class="form-group {{ $errors->has('kategori_id') ? 'has-error' : '' }}">
                     <label>Kategori</label>
                     <select class="form-control select-pilih" data-live-search="true" name="kategori_id" id="kategori_id" style="width: 100%">
                        <option disabled selected>Pilih Kategori</option>
                        @foreach($kategori as $data)
                        <option value="{{$data->id}}">{{$data->nama_kategori}}</option>
                        @endforeach
                     </select>
                     @if ($errors->has('kategori_id'))
                     <span class="help-block has-error kategori_id_error">
                        <strong>{{$errors->first('kategori_id')}}</strong>
                     </span>
                     @endif
                  </div>
                  <div class="form-group {{ $errors->has('supplier_id') ? 'has-error' : '' }}">
                     <label>Supplier</label>
                     <select class="form-control select-pilih" data-live-search="true" name="supplier_id" id="supplier_id" style="width: 100%">
                        <option disabled selected>Pilih Supplier</option>
                        @foreach($supplier as $data)
                        <option value="{{$data->id}}">{{$data->nama_supplier}}</option>
                        @endforeach
                     </select>
                     @if ($errors->has('suplier_id'))
                     <span class="help-block has-error supplier_id_error">
                        <strong>{{$errors->first('supplier_id')}}</strong>
                     </span>
                     @endif
                  </div>
                  <input type="hidden" name="id_pembuat" value="{{ Auth::user()->id}}">
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary" name="submit" id="aksiBarang">Simpan</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </form>
      </div>
    </div>
  </div>
</body>
</html> 