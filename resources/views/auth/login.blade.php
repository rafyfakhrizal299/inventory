
@extends('layouts.login')
@section('content')
<div class="limiter">
        <div class="container-login100">
            <div class="wrap-login100 p-l-85 p-r-85 p-t-55 p-b-55">
                <form class="login100-form validate-form flex-sb flex-w" method="POST" action="{{ route('login') }}">
                @csrf    
                    <span class="login100-form-title p-b-32">
                        Masuk Akun
                    </span>


                    <span class="txt1 p-b-11">
                        Alamat Surel ( <i>E-mail</i> )
                    </span>
                    <div class="wrap-input100 validate-input m-b-12" data-validate = "Enter username">
                        <input class="input100" type="email" name="email" {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                        <span class="focus-input100" data-placeholder="&#xf207;"></span>
                    </div>


                    <span class="txt1 p-b-11">
                        Kata Sandi
                    </span>
                    <div class="wrap-input100 validate-input m-b-12" data-validate="Enter password">
                        <input class="input100" type="password" name="password" {{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                        <span class="focus-input100" data-placeholder="&#xf191;"></span>
                    </div>
                    <div class="container-login100-form-btn">
                        <button class="login100-form-btn" type="submit">
                            Masuk
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="dropDownSelect1"></div>
@endsection