@extends('layouts.admin')
@section('content')
<br />
            <div class="col-md-12 col-sm-12 col-xs-12">
              <ul class="breadcrumb">
                <li><a href="home"><i class="fa fa-home"></i></a></li>
                <li>Barang</li>
              </ul>
            </div>
<div class="col-md-12 col-sm-12 col-xs-12" id="indexBarang">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><i class="fa fa-dropbox"></i> Data Barang </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                  	<button type="button" class="btn btn-success" id="TambahBarang"><i class="glyphicon glyphicon-plus"></i>Tambah Data</button>
                    <button type="button" class="btn btn-warning" id="Print" data-toggle="modal" data-target="#printBarang"><i class="glyphicon glyphicon-download"></i> Unduh Data</button>
                    @include('barang.modal')
                    @include('barang.print')
                  	<br>
                    <table class="table table-striped table-bordered" id="dataTableBarang">
                        <thead>
	                        <tr>
	                        	<th>Nama Barang</th>
                            <th>Jenis/Ukuran</th>
                            <th>Asal</th>
                            <th>Harga Beli</th>
                            <th>Harga Jual</th>
                            <th>Stok</th>
                            <th>Merk</th>
                            <th>Kategori</th>
                            <th>Supplier</th>
	                        	<th>Aksi</th>
	                    	</tr>
                    	</thead>
                    </table>
                  </div>
                </div>
              </div>
@endsection