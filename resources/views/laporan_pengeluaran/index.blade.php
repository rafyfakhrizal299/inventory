@extends('layouts.admin')
@section('content')
<br />
            <div class="col-md-12 col-sm-12 col-xs-12">
              <ul class="breadcrumb">
                <li><a href="home"><i class="fa fa-home"></i></a></li>
                <li>Laporan Pengeluaran</li>
              </ul>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12" id="indexLaporanPemasukan">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Data Laporan Pengeluaran </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <form action="{{url('/admin/laporan_pengeluaran_total')}}" method="post">
                    @csrf
                  <div class="x_content">
                  	<div class="col-md-12 col-sm-12 col-xs-12">
                      <div class="x_panel">
                  <div class="x_title">
                    <h4> Masukan Data Tanggal</h4>
                  <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div class="col-md-5 col-sm-5 col-xs-12">
                    <label>Dari Tanggal</label>
                      <input type="date" name="dari" class="form-control" required="">
                    </div>
                    <div class="col-md-5 col-sm-5 col-xs-12">
                      <label>Sampai Tanggal</label>
                      <input type="date" name="sampai" class="form-control" required="">
                    </div>
                    <br>
                      <div class="col-md-2 col-sm-2 col-xs-12">
                        <input type="submit" name="submit" class="btn btn-success" value="Filter">
                      </div>
                </div>
              </div>
            </div>
         
                    <br>
                    <!-- <div class="col-md-12 col-sm-12 col-xs-12">
                    <table class="table table-striped table-bordered">
                        <thead>
	                        <tr>
	                        	<th>No</th>
                            <th>Nama Barang</th>
                            <th>Jenis</th>
                            <th>Kuantitas</th>
                            <th>Harga</th>
                            <th>Nama Pembuat</th>
	                    	</tr>
                    	</thead>
                    </table>
                  </div> -->
                  </div>
                   </form>
                </div>
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Data Laporan Pengeluaran Hari Ini </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>  
                    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12" style="color: #a30909">
                      <div class="tile-stats" style="border-color: #a30909">
                        <div class="icon"><i class="fa fa-archive" style="color: #a30909"></i></div>
                        <div class="count">{{$barang_masuk->count('id')}}</div>
                        <h3 style="color: #a30909">Barang Masuk (Keseluruhan)</h3>
                        <p>Banyaknya Barang Masuk (Keseluruhan)</p>
                      </div>
                    </div>
                    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12" style="color: #222ed8">
                      <div class="tile-stats" style="border-color: #222ed8">
                        <div class="icon"><i class="fa fa-sort-amount-desc" style="color: #222ed8"></i></div>
                        <div class="count">{{$barang_masuk->sum('stok_masuk')}}</div>
                        <h3 style="color: #222ed8">Stok Masuk</h3>
                        <p>Total Stok Masuk</p>
                      </div>
                    </div>
                    <div class="animated flipInY col-lg-6 col-md-6 col-sm-6 col-xs-12" style="color: #06a320">
                      <div class="tile-stats" style="border-color: #06a320">
                        <div class="icon"><i class="fa fa-money" style="color: #06a320"></i></div>
                        <div class="count">Rp. {{number_format($barang_masuk->sum('total'),'2',',','.')}}</div>
                        <h3 style="color: #06a320">Pengeluaran</h3>
                        <p>Uang Yang Dikeluarkan</p>
                      </div>
                    </div>
                    <br>
                    <!-- <div class="col-md-2 col-sm-2 col-xs-12">
                        <button type="button" class="btn btn-warning"><i class="glyphicon glyphicon-download"></i>  Unduh Data</button>
                    </div> -->
                    <div class="col-md-12 col-sm-12 col-xs-12">
                    <table class="table table-striped table-bordered">
                        <thead>
                          <tr>
                            <th>No</th>
                            <th>Nama Barang</th>
                            <th>Jenis</th>
                            <th>Stok Masuk</th>
                            <th>Harga</th>
                            <th>Total</th>
                            <th>Nama Pembuat</th>
                        </tr>
                      </thead>
                      <?php
                          $no = 1;
                          ?>
                            @foreach($barang_masuk as $data)
                            <tbody>
                            <tr>
                              <td>{{$no++}}</td>
                              <td>{{$data->Barang->nama_barang}}</td>
                              <td>{{$data->Barang->jenis_ukuran}}</td>
                              <td>{{$data->stok_masuk}} {{$data->Barang->Satuan->nama_satuan}}</td>
                              <td>Rp. {{number_format($data->harga_beli,'2',',','.')}}</td>
                              <td>Rp. {{number_format($data->total,'2',',','.')}}</td>
                              <td>{{$data->User->name}}</td>
                            </tr>
                            </tbody>
                            @endforeach
                        <br>
                    </table>
                    <hr>
                    <h5><table table class="table mb-none">
                          <thead>
                          <tr>
                            <th>Total Barang Masuk : </th>
                          </tr>
                          </thead>
                          <tbody>
                            <td>
                              <li>{{$satuan_coba->sum('stok_masuk')}} <b>Coba</b></li>
                              <li>{{$satuan_coba2->sum('stok_masuk')}} <b>coba</b></li>
                            </td>
                            
                          </tbody>
                        </table>
                    </h5>
                  </div>
                  </div>
                </div>
              </div>
@endsection