@extends('layouts.admin')
@section('content')
<br />
            <div class="col-md-12 col-sm-12 col-xs-12">
              <ul class="breadcrumb">
                <li><i class="fa fa-home"></i> Home</li>
              </ul>
            </div>
              <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                  <div class="icon"><i class="fa fa-dropbox"></i></div>
                  <div class="count">{{$barang->count('id')}}</div>
                  <h3>Barang</h3>
                  <p>Total Barang Matrial</p>
                </div>
              </div>
              <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                  <div class="icon"><i class="fa fa-group"></i></div>
                  <div class="count">{{$supplier->count('id')}}</div>
                  <h3>Supplier</h3>
                  <p>Banyak Penyuplai Barang Matrial</p>
                </div>
              </div>
              <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                  <div class="icon"><i class="fa fa-sign-in"></i></div>
                  <div class="count">{{$barang_masuk->count('id')}}</div>
                  <h3>Barang Masuk</h3>
                  <p>Barang yang masuk per hari</p>
                </div>
              </div>
              <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                  <div class="icon"><i class="fa fa-sign-out"></i></div>
                  <div class="count">{{$barang_keluar->count('id')}}</div>
                  <h3>Barang Keluar</h3>
                  <p>Barang yang keluar per hari</p>
                </div>
              </div>
              <div class="animated flipInY col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="tile-stats">
                  <div class="icon"><i class="fa fa-money"></i></div>
                  <div class="count">{{$barang_masuk->count('id')}}</div>
                  <h3>Pendapatan Hari Ini</h3>
                  <p></p>
                </div>
              </div>
              <div class="animated flipInY col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="tile-stats">
                  <div class="icon"><i class="fa fa-money"></i></div>
                  <div class="count">{{$barang_keluar->count('id')}}</div>
                  <h3>Pengeluaran Hari Ini</h3>
                  <p></p>
                </div>
              </div>
@endsection