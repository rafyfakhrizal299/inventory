@extends('layouts.admin')
@section('content')
<br />
            <div class="col-md-12 col-sm-12 col-xs-12">
              <ul class="breadcrumb">
                <li><i class="fa fa-home"></i> Home</li>
              </ul>
            </div>
              <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12" style="color: #0eb8d6" >
                <div class="tile-stats" style="border-color: #000000">
                  <div class="icon"><i class="fa fa-dropbox" style="color: #0eb8d6"></i></div>
                  <div class="count">{{$barang->count('id')}}</div>
                  <h3 style="color: #0eb8d6">Barang</h3>
                  <p>Total Barang Matrial</p>
                </div>
              </div>
              <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12" style="color: #0eb8d6">
                <div class="tile-stats" style="border-color: #000000">
                  <div class="icon"><i class="fa fa-group" style="color: #0eb8d6"></i></div>
                  <div class="count">{{$supplier->count('id')}}</div>
                  <h3 style="color: #0eb8d6">Supplier</h3>
                  <p>Banyak Penyuplai Barang Matrial</p>
                </div>
              </div>
              <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12" style="color: #0eb8d6">
                <div class="tile-stats" style="border-color: #000000">
                  <div class="icon"><i class="fa fa-sign-in" style="color: #0eb8d6"></i></div>
                  <div class="count">{{$barang_masuk->sum('stok_masuk')}}</div>
                  <h3 style="color: #0eb8d6">Barang Masuk</h3>
                  <p>per hari</p>
                </div>
              </div>
              <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12" style="color: #0eb8d6">
                <div class="tile-stats" style="border-color: #000000">
                  <div class="icon"><i class="fa fa-sign-out" style="color: #0eb8d6"></i></div>
                  <div class="count">{{$order->sum('quantity')}}</div>
                  <h3 style="color: #0eb8d6">Barang Keluar</h3>
                  <p>Barang yang keluar per hari</p>
                </div>
              </div>
              <div class="animated flipInY col-lg-6 col-md-6 col-sm-6 col-xs-12" style="color: #0eb8d6">
                <div class="tile-stats" style="border-color: #000000">
                  <div class="icon"><i class="fa fa-money" style="color: #0eb8d6"></i></div>
                  <div class="count" style="color: #0eb8d6">Rp. {{number_format($barang_masuk->sum('total'),'2',',','.')}}</div>
                  <h3 style="color: #0eb8d6">Pengeluaran Hari Ini</h3>
                  <p></p>
                </div>
              </div>
              <div class="animated flipInY col-lg-6 col-md-6 col-sm-6 col-xs-12" style="color: #0eb8d6">
                <div class="tile-stats" style="border-color: #000000">
                  <div class="icon"><i class="fa fa-money" style="color: #0eb8d6"></i></div>
                  <div class="count" style="color: #0eb8d6">Rp. {{number_format($order_detail->sum('uang_muka'),'2',',','.')}}</div>
                  <h3 style="color: #0eb8d6">Pendapatan Hari Ini</h3>
                  <p></p>
                </div>
              </div>
              <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12" style="color: #a30909">
                      <div class="tile-stats" style="border-color: #000000">
                        <div class="icon"><i class="glyphicon glyphicon-remove" style="color: #a30909"></i></div>
                        <div class="count">{{$belum_bayar->count('id')}}</div>
                        <h3 style="color: #a30909">Belum Bayar</h3>
                        <p>Tagihan</p>
                      </div>
                    </div>
                    <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12" style="color: #222ed8">
                      <div class="tile-stats" style="border-color: #000000">
                        <div class="icon"><i class="glyphicon glyphicon-hourglass" style="color: #222ed8"></i></div>
                        <div class="count">{{$belum_lunas->count('id')}}</div>
                        <h3 style="color: #222ed8">Belum Lunas</h3>
                        <p>Tagihan</p>
                      </div>
                    </div>
                    <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12" style="color: #06a320">
                      <div class="tile-stats" style="border-color: #000000">
                        <div class="icon"><i class="glyphicon glyphicon-ok" style="color: #06a320"></i></div>
                        <div class="count">{{$sudah_lunas->count('id')}}</div>
                        <h3 style="color: #06a320">Sudah Lunas</h3>
                        <p>Tagihan</p>
                      </div>
                    </div>
              
@endsection