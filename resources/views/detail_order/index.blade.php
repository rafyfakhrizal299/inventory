@extends('layouts.admin')
@section('content')
<br />
            <div class="col-md-12 col-sm-12 col-xs-12">
              <ul class="breadcrumb">
                <li><a href="home"><i class="fa fa-home"></i></a></li>
                <li>Order</li>
              </ul>
            </div>
<div class="col-md-12 col-sm-12 col-xs-12 barangKeluar" id="indexOrder">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Data Order </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br>
                    <table class="table table-striped table-bordered" id="dataTableOrder">
                        <thead>
                          <tr>
                            <th>Nama Barang</th>
                            <th>Harga</th>
                            <th>uang Muka</th>
                            <th>Potongan Harga</th>
                            <th>Jumlah</th>
                            <th>Jumlah Total</th>
                            <th>Metode Pengiriman</th>
                            <th>Aksi</th>
                        </tr>
                      </thead>
                    </table>
                  </div>
                </div>
              </div>
              @endsection