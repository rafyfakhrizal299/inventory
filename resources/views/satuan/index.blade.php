@extends('layouts.admin')
@section('content')

<br />
            <div class="col-md-12 col-sm-12 col-xs-12">
              <ul class="breadcrumb">
                <li><a href="home"><i class="fa fa-home"></i></a></li>
                <li>Satuan</li>
              </ul>
            </div>
<div class="col-md-12 col-sm-12 col-xs-12" id="indexSatuan">
                <div class="alert alert-warning alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <strong>Perhatian !</strong> Di table satuan ini hanya memasukan satuan terkecil dalam sebuah sistem satuan toko bangunan , keterangan konversi satuan ada di halaman tambah order
            </div>
            <div id="success"></div>
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Data Satuan </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                  	<button type="button" class="btn btn-success" id="TambahSatuan">Tambah Data</button>
                    @include('satuan.modal')
                  	<br>
                    <table class="table table-striped table-bordered" id="dataTableSatuan">
                        <thead>
	                        <tr>
	                        	<th>Nama Satuan</th>
	                        	<th>Aksi</th>
	                    	</tr>
                    	</thead>
                    </table>
                  </div>
                </div>
              </div>
@endsection