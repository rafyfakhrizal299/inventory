<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Modal</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
<div class="modal fade" id="myModalSatuan" role="dialog">
    <div class="modal-dialog">
<div class="modal-content">
  <form id="formSatuan" method="post" enctype="multipart/form-data">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Masukan Data Satuan</h4>
        </div>
        <div class="modal-body">
          {{csrf_field()}} {{ method_field('POST') }}
          <input type="hidden" name="id" id="id">
          <div class="form-group">
                     <label>Nama Satuan</label>
                     <input type="text" name="nama_satuan" id="nama_satuan" class="form-control" 
                     placeholder="Nama Satuan">
                     <span class="help-block has-error nama_satuan_error"></span>
                  </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary" name="submit" id="aksi">Simpan</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </form>
      </div>
    </div>
  </div>
</body>
</html> 