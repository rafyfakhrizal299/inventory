<!-- The Modal -->
<div class="modal fade" id="printBarangKeluar" role="dialog">
  <div class="modal-dialog">
<div class="modal-content">
  
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><i class="glyphicon glyphicon-download"></i> Unduh Data</h4>
        </div>

      <!-- Modal body -->
      <div class="modal-body">
        <div class="modal-text text-center">
                            <p>Unduh <b>daftar barang</b> melalui?</p>
                            <a href="{{ url('admin/barangkeluar/downloadPDF/download')}}" type="button" class="mb-xs mt-xs mr-xs btn btn-danger">
                            <i class="fa fa-file-pdf-o"></i>&nbspPDF</a>
                            <a href="{{ route('export.barangkeluar')}}" type="button" class="mb-xs mt-xs mr-xs btn btn-success">
                            <i class="fa fa-file-excel-o"></i>&nbspExcel</a>
                          </div>
      </div>

    </div>
  </div>
</div>