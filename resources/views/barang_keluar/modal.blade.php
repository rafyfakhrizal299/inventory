<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Modal</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
<div class="modal fade" id="myModalBarangKeluar" role="dialog">
    <div class="modal-dialog">
<div class="modal-content">
  <form id="formBarangKeluar" method="post" enctype="multipart/form-data">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Masukan Data Barang</h4>
        </div>
        <div class="modal-body">
          {{csrf_field()}} {{ method_field('POST') }}
          <input type="hidden" name="id" id="id">
          <div class="form-group">
                     <label>Kuantitas</label>
                     <input type="text" name="kuantitas" id="kuantitas" class="form-control" 
                     placeholder="Nama Barang">
                     <span class="help-block has-error kuantitas_error"></span>
                  </div>
                  <div class="form-group">
                     <label>Harga</label>
                     <input type="text" onkeypress="return number(event)" name="harga" id="harga" class="form-control" 
                     placeholder="Harga Jual" required>
                     <input type="hidden" id="raw_harga_jual" name="harga_jual" value="">
                     <span class="help-block has-error harga_error"></span>
                  </div>
                  <input type="hidden" name="id_pembuat" value="{{ Auth::user()->id}}">
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary" name="submit" id="aksiBarangKeluar">Simpan</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </form>
      </div>
    </div>
  </div>
</body>
</html> 