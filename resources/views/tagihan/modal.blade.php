<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Modal</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
<div class="modal fade" id="myModalTagihan" role="dialog">
    <div class="modal-dialog">
<div class="modal-content">
  <form id="formTagihan" method="post" enctype="multipart/form-data">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Masukan Data Tagihan</h4>
        </div>
        <div class="modal-body">
          {{csrf_field()}} {{ method_field('POST') }}
          <input type="hidden" name="id" id="id">
          <div class="form-group">
                     <label>Uang Yang Dibayarkan</label>
                     <input type="text" name="down_payment" id="down_payment" class="form-control" 
                     placeholder="Dibayarkan" required>
                     <span class="help-block has-error down_payment_error"></span>
                  </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary" name="submit" id="aksiTagihan">Simpan</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </form>
      </div>
    </div>
  </div>
</body>
</html> 