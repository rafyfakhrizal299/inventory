@extends('layouts.admin')
@section('content')
<br />
            <div class="col-md-12 col-sm-12 col-xs-12">
              <ul class="breadcrumb">
                <li><a href="home"><i class="fa fa-home"></i></a></li>
                <li>Kategori</li>
              </ul>
            </div>
<div class="col-md-12 col-sm-12 col-xs-12" id="indexKategori">
  <div id="success"></div>
                <div class="x_panel">
                  <div class="x_title">
                    <h2><i class="fa fa-tag"></i> Data Kategori </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                  	<button type="button" class="btn btn-success" id="TambahKategori"><i class="fa fa-plus"></i> Tambah Data</button>
                    @include('kategori.modal')
                  	<br>
                    <table class="table table-striped table-bordered" id="dataTableKategori">
                        <thead>
	                        <tr>
	                        	<th>Nama Kategori</th>
	                        	<th>Aksi</th>
	                    	</tr>
                    	</thead>
                    </table>
                  </div>
                </div>
              </div>
@endsection