<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> Nama Kategori <span class="required"></span>
    </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" id="nama_kategori" name="nama_kategori" class="form-control col-md-7 col-xs-12" required>
            {!! $errors->first('nama_kategori', '<p class="help-block">:message</p>') !!}
        </div>
        	<div class="ln_solid"></div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button class="btn btn-primary" type="submit">Tambah</button>
                    </div>
                </div>
</div>