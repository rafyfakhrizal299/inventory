<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Modal</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
<div class="modal fade" id="myModalBarangMasuk" role="dialog">
    <div class="modal-dialog">
<div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Barang Masuk</h4>
        </div>
        <div class="modal-body">
          <div class="form-body" id="bm_edit">
          <input type="hidden" name="id" id="id">
                  <div class="form-group {{ $errors->has('barang_id') ? 'has-error' : '' }}">
                     <label>Nama Barang</label>
                     <select class="form-control select-pilih" data-live-search="true" name="barang_id" id="barang_id" style="width: 100%">
                        <option disabled selected>Pilih Barang</option>
                        @foreach($barang as $data)
                        <option value="{{$data->id}}">{{$data->nama_barang}}</option>
                        @endforeach
                     </select>
                     @if ($errors->has('barang_id'))
                     <span class="help-block has-error barang_id_error">
                        <strong>{{$errors->first('barang_id')}}</strong>
                     </span>
                     @endif
                  </div>
                  <div class="form-group">
                     <label>Stok Masuk</label>
                     <input type="text" name="stok_masuk" id="stok_masuk" class="form-control" 
                     placeholder="Stok Masuk" required>
                     <span class="help-block has-error stok_masuk_error"></span>
                  </div>
                  <div class="form-group">
                     <label>Harga Beli</label>
                     <input type="text" name="harga_beli" id="harga_beli" class="form-control" 
                     placeholder="Harga Beli" required>
                     <input type="hidden" id="raw_harga_beli" name="harga_beli" value="">
                     <span class="help-block has-error harga_beli_error"></span>
                  </div>
                  <input type="hidden" name="id_pembuat" value="{{ Auth::user()->id}}">
                  <div class="form-group {{ $errors->has('id_supplier') ? 'has-error' : '' }}">
                     <label>Supplier</label>
                     <select class="form-control select-pilih" data-live-search="true" name="id_supplier" id="id_supplier" style="width: 100%">
                        <option disabled selected>Pilih Supplier</option>
                        @foreach($supplier as $data)
                        <option value="{{$data->id}}">{{$data->nama_supplier}}</option>
                        @endforeach
                     </select>
                     @if ($errors->has('id_supplier'))
                     <span class="help-block has-error id_supplier_error">
                        <strong>{{$errors->first('id_supplier')}}</strong>
                     </span>
                     @endif
                  </div>
                </div>
        <div class="modal-footer">
          <button type="submit"  class="btn btn-primary" name="submitEdit" id="aksiEditBarangMasuk">Simpan</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
        </div>
      </div>
    </div>
</body>
</html> 