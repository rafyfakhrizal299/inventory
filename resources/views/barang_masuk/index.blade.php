@extends('layouts.admin')
@section('content')
<br />
            <div class="col-md-12 col-sm-12 col-xs-12">
              <ul class="breadcrumb">
                <li><a href="home"><i class="fa fa-home"></i></a></li>
                <li>Barang Masuk</li>
              </ul>
            </div>

            <div class="col-md-12 col-sm-12 col-xs-12 barangMasuk" id="indexBarangMasuk">
              <div id="success"></div>
                <div class="x_panel">
                  <div class="x_title">
                    <h2><i class="fa fa-sign-in"></i>  Data Barang Masuk </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <button type="button" class="btn btn-success" id="TambahBarangMasukChange"><i class="fa fa-plus"></i> Tambah Data</button>
                    <button type="button" class="btn btn-warning" id="Print"><i class="glyphicon glyphicon-download"></i> Unduh Data</button>
                    <table class="table table-striped table-bordered" id="dataTableBarangMasuk">
                        <thead>
                          <tr>
                            <th>Nama Barang Masuk</th>
                            <th>Stok Masuk</th>
                            <th>Harga Beli</th>
                            <th>Supplier</th>
                            <th>Tanggal Masuk</th>
                            <th>Total</th>
                            <th>Aksi</th>
                        </tr>
                      </thead>
                    </table>
                  </div>
                </div>
              </div>
<form id="formBarangMasuk" method="post" enctype="multipart/form-data" novalidate="">
  {{csrf_field()}} {{ method_field('POST') }}              
  @include('barang_masuk.create')
  @include('barang_masuk.modal')
</form>
@endsection
@section('js')
  <script>
  function addrow(){
      var no = $('#item_table tr').length;
      var html = '';
      html +='<tr id="row_'+no+'">';
      html +='<td><select name="barang_id[]" class="form-control select-pilih" id="barang_id"><option>--Pilih Barang--</option>@foreach($barang as $data)<option value="{{$data->id}}">{{$data->nama_barang}}-{{$data->Satuan->nama_satuan}}</option>@endforeach</select></td>';
      html +='<td><input type="text" name="stok_masuk[]" class="form-control"/></td>';
      html +='<td><input type="number" name="harga_beli[]" class="form-control" value=""/></td>';
      html +='<td><button type="button" class="btn btn-danger btn-sm" onclick="remove('+ no +')"><i class="fa fa-minus-square"></i></button></td></tr>';
      $('#last').after(html);
      $('.select-pilih').select2();
    }
    function remove(no){
      $('#row_'+no).remove();
    }
          // $("#supplier").change(function()
     //  {
     //    var id=$(this).val();
     //    $.ajax({
     //    type: 'GET',
     //    url: '',
     //    data: {id: id},
     //    cache: false,
     //    dataType:'json',
     //    success: function(data){
     //            $("input[name='id_supplier']").val(data.id_supplier);
     //            // $("input[name='nama_supplier']").val(data.nama_supplier);
     //            // $("input[name='no_telp']").val(data.no_telp);
     //            // $("input[name='alamat']").val(data.alamat);
     //            }
     //          });
     //  });
     $('.supplierada').change(function()
     {
      var sup_id=$(this).val();
      var dataId={'id':sup_id};
      console.log(dataId);
      $.ajax({
        type:'get',
        url:'{!!URL::to('findSupplier')!!}',
        dataType:'json',
        data : dataId,
        //return data will be json
        success:function(data){
          $("input[name='id_supplier']").val(data.id_supplier);
          $("input[name='no_telp']").val(data.no_telp);
          $("input[name='nama_supplier']").val(data.nama_supplier);
          $("input[name='nama_perusahaan']").val(data.nama_perusahaan);
          $("textarea[name='alamat']").val(data.alamat);

        },
        error:function(){

        }
      });
    });
      // $('.x_content').delegate('#supplier','change',function(){
      //   var tr=$(this).parent();
      //   var id =select.find('.supplier').val();
      //   var dataId={'id':id};
      //   console.log(dataId);
      //   $.ajax({
      //     type      :'GET',
      //     url       :'{!!URL::to('findprice')!!}',
      //     dataType  :'json',
      //     data      :dataId,
      //     success:function(data){
      //       $("input[name='id_supplier']").val(data.id_supplier);
      //       $("input[name='no_telp']").val(data.no_telp);
      //       $("input[name='nama_supplier']").val(data.nama_supplier);
      //       $("input[name='nama_perusahaan']").val(data.nama_perusahaan);
      //       $("textarea[name='alamat']").val(data.alamat);
      //     }
      //   });
      // });


  </script>
@endsection