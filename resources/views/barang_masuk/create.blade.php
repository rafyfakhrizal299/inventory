<div class="col-md-12 col-sm-12 col-xs-12" id="TambahMasuk">
                  <div id="success"></div>
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Tambah Barang Masuk </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><button type="button" class="form-control" id="TutupTambahMasuk">Batal</button></li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div class="col-md-12 col-sm-12 col-xs-12" id="SupplierMasuk">
                      <div class="x_panel">
                  <div class="x_title">
                    <h4> Data Supplier</h4>
                  <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div class="col-md-4 col-sm-4 col-xs-12">
                     <div class="form-group {{ $errors->has('id_supplier') ? 'has-error' : '' }}">
                     <label>Supplier</label>
                     <select class="form-control supplierada" id="supplierada" data-live-search="true" name="id_supplier"  style="width: 100%">
                        <option disabled selected>Pilih Supplier</option>
                        @foreach($supplier as $data)
                        <option value="{{$data->id}}">{{$data->nama_supplier}}</option>
                        @endforeach
                     </select>
                     @if ($errors->has('suplier_id'))
                     <span class="help-block has-error supplier_id_error">
                        <strong>{{$errors->first('id_supplier')}}</strong>
                     </span>
                     @endif
                  </div>

                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                  <div class="form-group">
                    <label>Nama Perusahaan</label>
                    <input type="text" name="nama_perusahaan" class="form-control" value="" readonly="">
                  </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                  <div class="form-group">
                    <label>No Telepon</label>
                    <input type="text" name="no_telp" class="form-control" value="" readonly="">
                  </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                  <div class="form-group">
                    <label>Alamat</label>
                    <textarea type="text" name="alamat" class="form-control" value="" readonly=""></textarea>
                  </div>
                </div>
                </div>
              </div>
            </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                       <div class="x_panel">
                        <div class="x_title">
                    <h2>Form Barang Masuk </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li>Tanggal Hari Ini {{ Carbon\carbon::now()->toDateString() }} </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                    <div class="x_content">
                    <div class="form-body" id="bm_create">
                    <table id="item_table" class="table table-striped table-bordered">
                        <thead>
                          <tr id="last">
                            <th>Nama Barang</th>
                            <th>Stok Masuk</th>
                            <th>Harga Beli</th>
                            <th>

                                <input type="hidden" name="id_supplier" value="">
                                <input type="hidden" name="id_pembuat" value="{{ Auth::user()->id }}">
                                <button type="button" name="add" class="btn btn-success btn-sm add" onclick="addrow()"><i class="fa fa-plus-square"></i></button></th>
                          </tr>
                      </thead>
                    </table>
                    <center><button type="submit" class="btn btn-primary" name="submit" id="aksiBarangMasuk"><i class="fa fa-check"></i> Simpan</button></center>
                  </div>
                </div>
              </div>
            </div>
                    </div>
                    </div>
                </div>