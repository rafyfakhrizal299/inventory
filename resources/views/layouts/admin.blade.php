<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="{{asset('assets/Backend/images/favicon.ico')}}" type="image/ico" />

    <title>TB. Makmur</title>

    <!-- Bootstrap -->
    <link href="{{asset('assets/Backend/vendors/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{asset('assets/Backend/vendors/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{asset('assets/Backend/vendors/nprogress/nprogress.css" rel="stylesheet')}}">
    <!-- iCheck -->
    <link href="{{asset('assets/Backend/vendors/iCheck/skins/flat/green.css" rel="stylesheet')}}">
	
    <!-- bootstrap-progressbar -->
    <link href="{{asset('assets/Backend/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css')}}" rel="stylesheet">
    <!-- JQVMap -->
    <link href="{{asset('assets/Backend/vendors/jqvmap/dist/jqvmap.min.css')}}" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="{{asset('assets/Backend/vendors/bootstrap-daterangepicker/daterangepicker.css')}}" rel="stylesheet">
    <!-- dataTable -->
    <link href="{{asset('/css/jquery.dataTables.css')}}" rel="stylesheet">
    <link href="{{asset('/css/dataTables.bootstrap.css')}}" rel="stylesheet">
     <link href="{{asset('/css/selectize.bootstrap3.css')}}" rel="stylesheet">
      <link href="{{asset('/css/selectize.css')}}" rel="stylesheet">
      <link href="{{asset('/css/custom.css')}}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

    <!-- Custom Theme Style -->
    <link href="{{asset('assets/Backend/build/css/custom.min.css')}}" rel="stylesheet">
    <script>
        function startTime() {
          var today = new Date();
          var h = today.getHours();
          var m = today.getMinutes();
          var s = today.getSeconds();
          m = checkTime(m);
          s = checkTime(s);
          document.getElementById('txt').innerHTML =
          h + ":" + m + ":" + s;
          var t = setTimeout(startTime, 500);
        }
        function checkTime(i) {
          if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
          return i;
        }
    </script>
  </head>

  <body class="nav-md" onload="startTime()">
    <div class="container body">
      <div class="main_container">
        @include('layouts.partials.admin.side')

        <!-- top navigation -->
        @include('layouts.partials.admin.nav')
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <!-- top tiles -->
          <!-- /top tiles -->

          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                @yield('content')
            </div>

          </div>
          <br />

        </div>
        
      </div>
    </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            TB. Makmur Jaya - Template Oleh Colorlib</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
    <!-- jQuery -->
    <script src="{{asset('assets/Backend/vendors/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('/js/jquery.dataTables.min.js')}}"></script>
    <!-- Bootstrap -->
    <script src="{{asset('assets/Backend/vendors/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <!-- FastClick -->
    <script src="{{asset('assets/Backend/vendors/fastclick/lib/fastclick.js')}}"></script>
    <!-- NProgress -->
    <script src="{{asset('assets/Backend/vendors/nprogress/nprogress.js')}}"></script>
    <!-- Chart.js -->
    <script src="{{asset('assets/Backend/vendors/Chart.js/dist/Chart.min.js')}}"></script>
    <!-- gauge.js -->
    <script src="{{asset('assets/Backend/vendors/gauge.js/dist/gauge.min.js')}}"></script>
    <!-- bootstrap-progressbar -->
    <script src="{{asset('assets/Backend/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js')}}"></script>
    <!-- iCheck -->
    <script src="{{asset('assets/Backend/vendors/iCheck/icheck.min.js')}}"></script>
    <!-- Skycons -->
    <script src="{{asset('assets/Backend/vendors/skycons/skycons.js')}}"></script>
    <!-- Flot -->
    <script src="{{asset('assets/Backend/vendors/Flot/jquery.flot.js')}}"></script>
    <script src="{{asset('assets/Backend/vendors/Flot/jquery.flot.pie.js')}}"></script>
    <script src="{{asset('assets/Backend/vendors/Flot/jquery.flot.time.js')}}"></script>
    <script src="{{asset('assets/Backend/vendors/Flot/jquery.flot.stack.js')}}"></script>
    <script src="{{asset('assets/Backend/vendors/Flot/jquery.flot.resize.js')}}"></script>
    <!-- Flot plugins -->
    <script src="{{asset('assets/Backend/vendors/flot.orderbars/js/jquery.flot.orderBars.js')}}"></script>
    <script src="{{asset('assets/Backend/vendors/flot-spline/js/jquery.flot.spline.min.js')}}"></script>
    <script src="{{asset('assets/Backend/vendors/flot.curvedlines/curvedLines.js')}}"></script>
    <!-- DateJS -->
    <script src="{{asset('assets/Backend/vendors/DateJS/build/date.js')}}"></script>
    <!-- JQVMap -->
    <script src="{{asset('/js/dataTables.bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script> 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src="{{asset('/js/selectize.min.js')}}"></script>
    <!-- Custom Theme Scripts -->

    <script src="{{asset('assets/Backend/build/js/custom.min.js')}}"></script>
    <script src="{{asset('js/kategori.js')}}"></script>
    <script src="{{asset('js/satuan.js')}}"></script>
    <script src="{{asset('js/supplier.js')}}"></script>
    <script src="{{asset('js/merk.js')}}"></script>
    <script src="{{asset('js/barang.js')}}"></script>
    <script src="{{asset('js/barang_masuk.js')}}"></script>
    <script src="{{asset('js/barang_keluar.js')}}"></script>
    <script src="{{asset('js/customaja.js')}}"></script>
    <script src="{{asset('js/global.js')}}"></script>
    <script src="{{asset('js/order.js')}}"></script>
    <script src="{{asset('js/tagihan.js')}}"></script>
    <script src="{{asset('js/user.js')}}"></script>
    <script src="{{asset('js/order_detail.js')}}"></script>
    @yield('js')
  </body>
</html>
