<div class="col-md-3 left_col" >
  <div class="left_col scroll-view">
    <div class="navbar nav_title" style="border: 0;">
      <a href="index.html" class="site_title"><i class="fa fa-home"></i> <span> TB. Makmur R.</span></a>
    </div>
    <div class="clearfix"></div>

    <!-- menu profile quick info -->
    <div class="profile clearfix">
      <div class="profile_pic">
        <img src="{{asset('assets/Backend/production/images/img.jpg')}}" alt="..." class="img-circle profile_img">
      </div>
      <div class="profile_info">
        <span>Selamat Datang,</span>
        <h2>{{ Auth::user()->name }}</h2>
      </div>
    </div>
    <!-- /menu profile quick info -->
    <br />
    <!-- sidebar menu --> 
    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
      @role('Admin')
      <div class="menu_section">
        <ul class="nav side-menu">
          <li><a href="/home"><i class="fa fa-dashboard"></i> Dashboard </a></li>
        </ul>
      </div>
      <div class="menu_section">
        <h3>Master Data</h3>
        <ul class="nav side-menu">
          <li><a href="/admin/barang"><i class="fa fa-archive"></i> Barang </a></li>
          <li><a href="/admin/merk"><i class="fa fa-copyright"></i> Merk Barang </a></li>
          <li><a href="/admin/supplier"><i class="fa fa-group"></i> Supplier</a></li>
          <li><a href="/admin/kategori"><i class="fa fa-tag"></i> Kategori </a></li>
          <li><a href="/admin/satuan"><i class="fa fa-circle"></i> Satuan </a></li>
        </ul>
      </div>
      <div class="menu_section">
        <h3>Keluar Masuk</h3>
        <ul class="nav side-menu">
          <li><a href="/admin/barangmasuk"><i class="fa fa-sign-in"></i> Barang Masuk </a></li>
          <li><a href="/admin/barangkeluar"><i class="fa fa-sign-out"></i> Barang Keluar </a></li>
        </ul>
      </div>
      <div class="menu_section">
        <h3>Laporan</h3>
        <ul class="nav side-menu">
          <li><a href="/admin/laporan_pemasukan"><i class="fa fa-file-text"></i> Laporan Pemasukan </a></li>
          <li><a href="/admin/laporan_pengeluaran"><i class="fa fa-file-text"></i> Laporan Pengeluaran </a>
          </li>
        </ul>
      </div>
      <div class="menu_section">
        <h3>Transaksi</h3>
        <ul class="nav side-menu">
          <li><a href="/admin/order"><i class="fa fa-shopping-cart"></i> Order </a></li>
          <li><a href="/admin/tagihan"><i class="fa fa-money"></i> Tagihan </a></li>
          <li><a href="/admin/order_detail"><i class="fa fa-list-alt"></i> Nota </a></li>
        </ul>
      </div>
      <div class="menu_section">
        <h3>User</h3>
        <ul class="nav side-menu">
          <li><a href="/admin/user"><i class="fa fa-user"></i> Data User </a></li>
        </ul>
      </div>
      @endrole
      @role('Karyawan')
      <div class="menu_section">
        <h3>Keluar Masuk</h3>
        <ul class="nav side-menu">
          <li><a href="/karyawan/barangmasuk"><i class="fa fa-sign-in"></i> Barang Masuk </a></li>
          <li><a href="/karyawan/barangkeluar"><i class="fa fa-sign-out"></i> Barang Keluar </a></li>
        </ul>
      </div>
      <div class="menu_section">
        <h3>Transaksi</h3>
        <ul class="nav side-menu">
          <li><a href="/karyawan/order"><i class="fa fa-shopping-cart"></i> Order </a></li>
          <li><a href="/karyawan/tagihan"><i class="fa fa-money"></i> Tagihan </a></li>
        </ul>
      </div>
    @endrole
    </div> 
    <!-- /sidebar menu -->
    <!-- /menu footer buttons -->
    <div class="sidebar-footer hidden-small">
       <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
    </div>
    <!-- /menu footer buttons -->
  </div>
</div>