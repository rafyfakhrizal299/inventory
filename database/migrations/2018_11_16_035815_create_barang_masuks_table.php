<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBarangMasuksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('barang_masuks', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('barang_id');
            $table->double('stok_masuk');
            $table->double('harga_beli');
            $table->foreign('barang_id')->on('barangs')->references('id')->onDelete('cascade');
            $table->unsignedInteger('id_pembuat');
            $table->foreign('id_pembuat')->on('users')->references('id')->onDelete('cascade');
            $table->unsignedInteger('id_supplier');
            $table->foreign('id_supplier')->on('suppliers')->references('id')->onDelete('cascade');
            $table->date('tanggal_masuk')->nullable();
            $table->unsignedInteger('total');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('barang_masuks');
    }
}
