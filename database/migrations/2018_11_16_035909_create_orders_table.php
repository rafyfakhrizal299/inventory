<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_pembuat');
            $table->foreign('id_pembuat')
                  ->references('id')
                  ->on('users')
                  ->onDelete('CASCADE');
            $table->unsignedInteger('barang_id');
            $table->foreign('barang_id')
                  ->references('id')
                  ->on('barangs')
                  ->onDelete('CASCADE');
            $table->text('nama_pembeli');
            $table->text('alamat');
            $table->text('no_telepon');
            $table->date('tanggal_beli');
            $table->integer('quantity');
            $table->integer('harga');
            $table->integer('jumlah');
            $table->integer('total_jumlah')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
