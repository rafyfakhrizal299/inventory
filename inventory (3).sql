-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Waktu pembuatan: 05 Apr 2019 pada 07.57
-- Versi server: 10.1.35-MariaDB
-- Versi PHP: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `inventory`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `barangs`
--

CREATE TABLE `barangs` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama_barang` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis_asal` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `harga_beli` int(11) NOT NULL,
  `harga_jual` int(11) NOT NULL,
  `stok` int(11) NOT NULL,
  `satuan_id` int(10) UNSIGNED NOT NULL,
  `merk_id` int(10) UNSIGNED NOT NULL,
  `kategori_id` int(10) UNSIGNED NOT NULL,
  `supplier_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `barangs`
--

INSERT INTO `barangs` (`id`, `nama_barang`, `jenis_asal`, `harga_beli`, `harga_jual`, `stok`, `satuan_id`, `merk_id`, `kategori_id`, `supplier_id`, `created_at`, `updated_at`) VALUES
(1, 'Avian Biru', '20 ml', 1700000, 1700000, -6, 1, 1, 2, 2, NULL, '2019-04-05 02:58:53'),
(2, 'Avian Kuning Besar 200 ml', '-', 20000, 30000, 17, 1, 1, 2, 2, '2019-04-05 03:08:46', '2019-04-05 03:08:46');

-- --------------------------------------------------------

--
-- Struktur dari tabel `barang_keluars`
--

CREATE TABLE `barang_keluars` (
  `id` int(10) UNSIGNED NOT NULL,
  `barang_id` int(10) UNSIGNED NOT NULL,
  `id_pembuat` int(10) UNSIGNED NOT NULL,
  `stok_keluar` int(11) NOT NULL,
  `harga_jual` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `barang_masuks`
--

CREATE TABLE `barang_masuks` (
  `id` int(10) UNSIGNED NOT NULL,
  `barang_id` int(10) UNSIGNED NOT NULL,
  `stok_masuk` double NOT NULL,
  `harga_beli` double NOT NULL,
  `id_pembuat` int(10) UNSIGNED NOT NULL,
  `id_supplier` int(10) UNSIGNED NOT NULL,
  `tanggal_masuk` date DEFAULT NULL,
  `total` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `barang_masuks`
--

INSERT INTO `barang_masuks` (`id`, `barang_id`, `stok_masuk`, `harga_beli`, `id_pembuat`, `id_supplier`, `tanggal_masuk`, `total`, `created_at`, `updated_at`) VALUES
(1, 1, 2, 13000, 1, 2, NULL, 26000, '2019-01-24 00:44:03', '2019-01-24 00:44:03');

-- --------------------------------------------------------

--
-- Struktur dari tabel `costumers`
--

CREATE TABLE `costumers` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `foto_barangs`
--

CREATE TABLE `foto_barangs` (
  `id` int(10) UNSIGNED NOT NULL,
  `gambar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `barang_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `info_tokos`
--

CREATE TABLE `info_tokos` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategoris`
--

CREATE TABLE `kategoris` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama_kategori` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `kategoris`
--

INSERT INTO `kategoris` (`id`, `nama_kategori`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Lemesan', NULL, '2019-01-23 13:58:49', '2019-01-23 13:58:49'),
(2, 'Cat', NULL, '2019-01-23 13:58:58', '2019-01-23 13:58:58'),
(3, 'Pasir', NULL, '2019-01-23 13:59:08', '2019-01-23 13:59:08');

-- --------------------------------------------------------

--
-- Struktur dari tabel `laporan_pemasukans`
--

CREATE TABLE `laporan_pemasukans` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama_pembeli` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `laporan_pengeluarans`
--

CREATE TABLE `laporan_pengeluarans` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `merks`
--

CREATE TABLE `merks` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama_merk` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `merks`
--

INSERT INTO `merks` (`id`, `nama_merk`, `created_at`, `updated_at`) VALUES
(1, 'Avian', '2019-01-23 14:01:32', '2019-01-23 14:01:32'),
(2, 'Avitex', '2019-01-23 14:01:38', '2019-01-23 14:01:38'),
(3, 'Pasir', '2019-01-23 14:01:44', '2019-01-23 14:01:44'),
(4, 'Jaguar', '2019-01-23 14:01:57', '2019-01-23 14:01:57');

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_06_05_040218_laratrust_setup_tables', 1),
(4, '2018_11_16_020940_create_kategoris_table', 1),
(5, '2018_11_16_021010_create_suppliers_table', 1),
(6, '2018_11_16_021011_create_merks_table', 1),
(7, '2018_11_16_021022_create_satuans_table', 1),
(8, '2018_11_16_021051_create_barangs_table', 1),
(9, '2018_11_16_021052_create_foto_barangs_table', 1),
(10, '2018_11_16_035815_create_barang_masuks_table', 1),
(11, '2018_11_16_035900_create_barang_keluars_table', 1),
(12, '2018_11_16_035909_create_orders_table', 1),
(13, '2018_11_16_035938_create_laporan_pemasukans_table', 1),
(14, '2018_11_16_040000_create_laporan_pengeluarans_table', 1),
(15, '2018_11_16_040636_create_info_tokos_table', 1),
(16, '2018_11_16_040855_create_costumers_table', 1),
(17, '2018_11_16_213944_detail_order', 1),
(18, '2018_12_05_100744_create_tagihans_table', 1),
(19, '2019_01_21_111041_create_tokos_table', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_pembuat` int(10) UNSIGNED NOT NULL,
  `barang_id` int(10) UNSIGNED NOT NULL,
  `nama_pembeli` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_telepon` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_beli` date NOT NULL,
  `quantity` int(11) NOT NULL,
  `harga` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `total_jumlah` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `orders`
--

INSERT INTO `orders` (`id`, `id_pembuat`, `barang_id`, `nama_pembeli`, `alamat`, `no_telepon`, `tanggal_beli`, `quantity`, `harga`, `jumlah`, `total_jumlah`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Rafy', 'Kp.Curug Dog Dog', '089625838303', '2019-01-23', 2, 17000, 34000, NULL, '2019-01-23 14:05:16', '2019-01-23 14:05:16'),
(2, 1, 1, 'Ahmad', 'JJAKJ', '0909008', '2019-01-23', 2, 17000, 34000, NULL, '2019-01-23 14:12:07', '2019-01-23 14:12:07'),
(3, 1, 1, 'Rafy', 'JJAKJ', '0909008', '2019-01-23', 0, 17000, 0, NULL, '2019-01-23 14:16:02', '2019-01-23 14:16:02'),
(4, 1, 1, 'AKu', 'JJAKJ', '0909008', '2019-01-23', 2, 17000, 34000, NULL, '2019-01-23 14:17:28', '2019-01-23 14:17:28'),
(5, 1, 1, 'Ahmad', 'JJAKJ', '0909008', '2019-01-23', 1, 17000, 17000, NULL, '2019-01-23 14:25:40', '2019-01-23 14:25:40'),
(6, 1, 1, 'Saya', 'JJAKJ', '0909008', '2019-01-23', 2, 17000, 34000, NULL, '2019-01-23 14:26:33', '2019-01-23 14:26:33'),
(7, 1, 1, 'AKu', 'JJAKJ', '0909008', '2019-01-23', 2, 17000, 34000, NULL, '2019-01-23 14:28:18', '2019-01-23 14:28:18'),
(8, 1, 1, 'Ahmad', 'JJAKJ', '0909008', '2019-01-23', 1, 17000, 17000, NULL, '2019-01-23 14:30:41', '2019-01-23 14:30:41'),
(9, 1, 1, 'Rafy', 'JJAKJ', '0909008', '2019-01-23', 2, 17000, 34000, NULL, '2019-01-23 14:35:44', '2019-01-23 14:35:44'),
(10, 1, 1, 'Ahmad', 'JJAKJ', '0909008', '2019-01-23', 2, 17000, 34000, NULL, '2019-01-23 14:36:32', '2019-01-23 14:36:32'),
(11, 1, 1, 'Rafy', 'JJAKJ', '0909008', '2019-01-23', 2, 17000, 34000, NULL, '2019-01-23 14:53:44', '2019-01-23 14:53:44'),
(12, 1, 1, 'Rafy', 'JJAKJ', '089625838303', '2019-02-11', 2, 17000, 34000, NULL, '2019-02-11 06:40:32', '2019-02-11 06:40:32'),
(13, 1, 1, 'Atan', 'Kp.Arab', '089262626266', '2019-03-17', 2, 17000, 34000, NULL, '2019-03-17 02:35:50', '2019-03-17 02:35:50'),
(14, 1, 1, 'ereew', '3efed', '4343', '2019-03-17', 2, 17000, 34000, NULL, '2019-03-17 02:36:50', '2019-03-17 02:36:50'),
(15, 1, 1, 'dsas', 'ssss', '0292929', '2019-03-17', 1, 17000, 17000, NULL, '2019-03-17 02:37:17', '2019-03-17 02:37:17'),
(16, 1, 1, 'dsas', 'fbb', '0292929', '2019-03-17', 2, 17000, 34000, NULL, '2019-03-17 02:41:11', '2019-03-17 02:41:11'),
(17, 1, 1, 'dsas', 'fsfsd', '67564446', '2019-03-17', 2, 17000, 34000, NULL, '2019-03-17 02:41:49', '2019-03-17 02:41:49'),
(18, 1, 1, 'dsas', 'ss', '67564446', '2019-03-17', 2, 17000, 34000, NULL, '2019-03-17 02:55:30', '2019-03-17 02:55:30'),
(19, 1, 1, 'dsas', 'ss', '99201', '2019-03-17', 2, 17000, 34000, NULL, '2019-03-17 02:56:37', '2019-03-17 02:56:37'),
(20, 1, 1, 'dsas', 'asdsa', '0292929', '2019-03-17', 2, 17000, 34000, NULL, '2019-03-17 02:57:25', '2019-03-17 02:57:25'),
(21, 1, 1, 'dsas', 'sss', '2', '2019-03-17', 2, 17000, 34000, NULL, '2019-03-17 02:59:40', '2019-03-17 02:59:40'),
(22, 1, 1, 'dsas', 's', '323', '2019-03-17', 2, 17000, 34000, NULL, '2019-03-17 03:01:27', '2019-03-17 03:01:27'),
(23, 1, 1, 'dsas', 'Kp. Curug Dog DOg', '0292929', '2019-04-02', 2, 17000, 34000, NULL, '2019-04-02 11:59:59', '2019-04-02 11:59:59'),
(24, 1, 1, 'dsas', 'rafy1234', '67564446', '2019-04-04', 2, 1700000, 3400000, NULL, '2019-04-04 12:14:41', '2019-04-04 12:14:41'),
(25, 1, 1, 'dsas', 'dsnsanas', '0292929', '2019-04-05', 2, 1700000, 3400000, NULL, '2019-04-05 01:57:33', '2019-04-05 01:57:33'),
(26, 1, 1, 'dsas', 'wadas', '67564446', '2019-04-05', 2, 1700000, 3400000, NULL, '2019-04-05 02:00:10', '2019-04-05 02:00:10'),
(27, 1, 1, 'dsas', 'asda', '67564446', '2019-04-05', 1, 1700000, 1700000, NULL, '2019-04-05 02:12:58', '2019-04-05 02:12:58'),
(28, 1, 1, 'RafyFH', 'RafyFh', '089625838303', '2019-04-05', 0, 1700000, 0, NULL, '2019-04-05 02:14:31', '2019-04-05 02:14:31'),
(29, 1, 1, 'Samri', 'Bismillah', '089726262667', '2019-04-05', 2, 1700000, 3400000, NULL, '2019-04-05 02:16:11', '2019-04-05 02:16:11'),
(30, 1, 1, 'Danag', 'SJSJJ', '02801920', '2019-04-05', 1, 1700000, 1700000, NULL, '2019-04-05 02:18:13', '2019-04-05 02:18:13'),
(31, 1, 1, 'RafyFH', 'rafysgjah', '092920', '2019-04-05', 1, 1700000, 1700000, NULL, '2019-04-05 02:25:18', '2019-04-05 02:25:18'),
(32, 1, 1, 'dsas', 'Kkakak', '222', '2019-04-05', 1, 1700000, 1700000, NULL, '2019-04-05 02:28:12', '2019-04-05 02:28:12'),
(33, 1, 1, 'Samri', 'assda', '020202', '2019-04-05', 2, 1700000, 3400000, NULL, '2019-04-05 02:29:16', '2019-04-05 02:29:16'),
(34, 1, 1, 'RafyFH', 'rafy', '089625838303', '2019-04-05', 2, 1700000, 3400000, NULL, '2019-04-05 02:37:53', '2019-04-05 02:37:53'),
(35, 1, 1, 'RafyFH', 'rafyaf', '67564446', '2019-04-05', 2, 1700000, 3400000, NULL, '2019-04-05 02:44:55', '2019-04-05 02:44:55'),
(36, 1, 1, 'RafyFH', 'rafy', '67564446', '2019-04-05', 2, 1700000, 3400000, NULL, '2019-04-05 02:46:22', '2019-04-05 02:46:22'),
(37, 1, 1, 'dsas', 'rafy', '67564446', '2019-04-05', 1, 1700000, 1700000, NULL, '2019-04-05 02:52:49', '2019-04-05 02:52:49'),
(38, 1, 1, 'dsas', 'down', '67564446', '2019-04-05', 1, 1700000, 1700000, NULL, '2019-04-05 02:53:56', '2019-04-05 02:53:56'),
(39, 1, 1, 'dsas', 'arfyaf', '67564446', '2019-04-05', 1, 1700000, 1700000, NULL, '2019-04-05 02:58:53', '2019-04-05 02:58:53');

-- --------------------------------------------------------

--
-- Struktur dari tabel `order_details`
--

CREATE TABLE `order_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `kode_order` int(11) NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `metode_pengiriman` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uang_muka` int(11) NOT NULL,
  `potongan_harga` int(11) NOT NULL,
  `sub_total` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `order_details`
--

INSERT INTO `order_details` (`id`, `kode_order`, `order_id`, `metode_pengiriman`, `uang_muka`, `potongan_harga`, `sub_total`, `total`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'dikirim', 34000, 0, 34000, 34000, '2019-01-23 14:05:16', '2019-01-23 14:05:16'),
(2, 2, 2, 'dikirim', 25000, 0, 34000, 34000, '2019-01-23 14:12:07', '2019-01-23 14:12:07'),
(3, 3, 3, 'dikirim', 0, 0, 34000, 34000, '2019-01-23 14:16:03', '2019-01-23 14:16:03'),
(4, 4, 4, 'dikirim', 0, 0, 34000, 34000, '2019-01-23 14:17:28', '2019-01-23 14:17:28'),
(5, 5, 5, 'dikirim', 8000, 0, 17000, 17000, '2019-01-23 14:25:41', '2019-01-23 14:25:41'),
(6, 6, 6, 'dikirim', 34000, 0, 34000, 34000, '2019-01-23 14:26:33', '2019-01-23 14:26:33'),
(7, 7, 7, 'dibawa sendiri', 34000, 0, 34000, 34000, '2019-01-23 14:28:18', '2019-01-23 14:28:18'),
(8, 8, 8, 'dikirim', 17000, 0, 17000, 17000, '2019-01-23 14:30:42', '2019-01-23 14:30:42'),
(9, 9, 9, 'dikirim', 34000, 0, 34000, 34000, '2019-01-23 14:35:44', '2019-01-23 14:35:44'),
(10, 10, 10, 'dikirim', 33000, 0, 34000, 34000, '2019-01-23 14:36:32', '2019-01-23 14:36:32'),
(11, 11, 11, 'dikirim', 32000, 0, 34000, 34000, '2019-01-23 14:53:44', '2019-01-23 14:53:44'),
(12, 12, 12, 'dikirim', 34000, 0, 34000, 34000, '2019-02-11 06:40:32', '2019-02-11 06:40:32'),
(13, 13, 13, 'dikirim', 0, 0, 34000, 34000, '2019-03-17 02:35:51', '2019-03-17 02:35:51'),
(14, 14, 14, 'dikirim', 30000, 0, 34000, 34000, '2019-03-17 02:36:50', '2019-03-17 02:36:50'),
(15, 15, 15, 'dikirim', 17000, 0, 17000, 17000, '2019-03-17 02:37:17', '2019-03-17 02:37:17'),
(16, 16, 16, 'dikirim', 34000, 0, 34000, 34000, '2019-03-17 02:41:11', '2019-03-17 02:41:11'),
(17, 17, 17, 'dikirim', 32000, 0, 34000, 34000, '2019-03-17 02:41:50', '2019-03-17 02:41:50'),
(18, 18, 18, 'dikirim', 34000, 0, 34000, 34000, '2019-03-17 02:55:30', '2019-03-17 02:55:30'),
(19, 19, 19, 'dikirim', 34000, 0, 34000, 34000, '2019-03-17 02:56:37', '2019-03-17 02:56:37'),
(20, 20, 20, 'dikirim', 24000, 0, 34000, 34000, '2019-03-17 02:57:25', '2019-03-17 02:57:25'),
(21, 21, 21, 'dikirim', 32000, 0, 34000, 34000, '2019-03-17 02:59:40', '2019-03-17 02:59:40'),
(22, 22, 22, 'dikirim', 32000, 0, 34000, 34000, '2019-03-17 03:01:28', '2019-03-17 03:01:28'),
(23, 23, 23, 'dikirim', 14000, 20000, 34000, -6000, '2019-04-02 11:59:59', '2019-04-02 11:59:59'),
(24, 24, 24, 'dikirim', 1400000, 2000000, 3400000, 1400000, '2019-04-04 12:14:41', '2019-04-04 12:14:41'),
(25, 25, 25, 'dikirim', 3400000, 0, 3400000, 3400000, '2019-04-05 01:57:34', '2019-04-05 01:57:34'),
(26, 26, 26, 'dikirim', 3400000, 0, 3400000, 3400000, '2019-04-05 02:00:10', '2019-04-05 02:00:10'),
(27, 27, 27, 'dikirim', 1700000, 0, 1700000, 1700000, '2019-04-05 02:12:58', '2019-04-05 02:12:58'),
(28, 28, 28, 'dikirim', 3400000, 0, 3400000, 3400000, '2019-04-05 02:14:31', '2019-04-05 02:14:31'),
(29, 29, 29, 'dikirim', 3400000, 0, 3400000, 3400000, '2019-04-05 02:16:11', '2019-04-05 02:16:11'),
(30, 30, 30, 'dikirim', 1700000, 0, 1700000, 1700000, '2019-04-05 02:18:13', '2019-04-05 02:18:13'),
(31, 31, 31, 'dikirim', 1700000, 0, 1700000, 1700000, '2019-04-05 02:25:18', '2019-04-05 02:25:18'),
(32, 32, 32, 'dikirim', 1700000, 0, 1700000, 1700000, '2019-04-05 02:28:12', '2019-04-05 02:28:12'),
(33, 33, 33, 'dikirim', 3400000, 0, 3400000, 3400000, '2019-04-05 02:29:16', '2019-04-05 02:29:16'),
(34, 34, 34, 'dikirim', 3400000, 0, 3400000, 3400000, '2019-04-05 02:37:53', '2019-04-05 02:37:53'),
(35, 35, 35, 'dikirim', 1700000, 0, 3400000, 3400000, '2019-04-05 02:44:55', '2019-04-05 02:44:55'),
(36, 36, 36, 'dikirim', 3400000, 0, 3400000, 3400000, '2019-04-05 02:46:22', '2019-04-05 02:46:22'),
(37, 37, 37, 'dikirim', 1700000, 0, 1700000, 1700000, '2019-04-05 02:52:50', '2019-04-05 02:52:50'),
(38, 38, 38, 'dikirim', 119818, 0, 1700000, 1700000, '2019-04-05 02:53:57', '2019-04-05 02:53:57'),
(39, 39, 39, 'dikirim', 1700000, 0, 1700000, 1700000, '2019-04-05 02:58:53', '2019-04-05 02:58:53');

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `permission_user`
--

CREATE TABLE `permission_user` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `user_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'Admin', NULL, '2019-01-23 13:57:24', '2019-01-23 13:57:24'),
(2, 'Karyawan', 'Karyawan', NULL, '2019-01-23 13:57:25', '2019-01-23 13:57:25');

-- --------------------------------------------------------

--
-- Struktur dari tabel `role_user`
--

CREATE TABLE `role_user` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `user_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `role_user`
--

INSERT INTO `role_user` (`role_id`, `user_id`, `user_type`) VALUES
(1, 1, 'App\\User'),
(2, 2, 'App\\User'),
(2, 3, 'App\\User'),
(2, 4, 'App\\User'),
(2, 5, 'App\\User');

-- --------------------------------------------------------

--
-- Struktur dari tabel `satuans`
--

CREATE TABLE `satuans` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama_satuan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `satuans`
--

INSERT INTO `satuans` (`id`, `nama_satuan`, `created_at`, `updated_at`) VALUES
(1, 'Pcs', '2019-01-23 13:58:07', '2019-01-23 13:58:07'),
(2, 'Roda', '2019-01-23 13:58:20', '2019-01-23 13:58:20'),
(3, 'Sak', '2019-03-17 02:26:21', '2019-03-17 02:26:21'),
(4, 'Kg', '2019-03-17 02:26:43', '2019-03-17 02:26:43');

-- --------------------------------------------------------

--
-- Struktur dari tabel `suppliers`
--

CREATE TABLE `suppliers` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama_supplier` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_perusahaan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_telp` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `suppliers`
--

INSERT INTO `suppliers` (`id`, `nama_supplier`, `nama_perusahaan`, `alamat`, `no_telp`, `created_at`, `updated_at`) VALUES
(1, 'Tukang Pasir', 'PT.CumaSatu', 'Curug Dog DOg', '0909008', '2019-01-23 13:59:39', '2019-01-23 13:59:39'),
(2, 'Avian', 'PT.Avian Maju', 'Jl.Soekarno Hatta', '089764344', '2019-01-23 14:00:08', '2019-01-23 14:00:08'),
(3, 'Jaguar', 'PT Indo Biokimia Banten', 'Serang , Banten', '089767899999', '2019-01-23 14:01:15', '2019-01-23 14:01:15');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tagihans`
--

CREATE TABLE `tagihans` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_detail_id` int(10) UNSIGNED NOT NULL,
  `status` tinyint(1) NOT NULL,
  `total_pembayaran` int(11) NOT NULL,
  `sisa_pembayaran` int(11) NOT NULL,
  `jumlah_pembayaran` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `tagihans`
--

INSERT INTO `tagihans` (`id`, `order_detail_id`, `status`, `total_pembayaran`, `sisa_pembayaran`, `jumlah_pembayaran`, `created_at`, `updated_at`) VALUES
(9, 9, 1, 34000, 0, 34000, '2019-01-23 14:35:44', '2019-01-23 14:35:44'),
(10, 10, 0, 34000, 33000, 34000, '2019-01-23 14:36:32', '2019-01-23 14:44:27'),
(11, 11, 1, 34000, 0, 34000, '2019-01-23 14:53:44', '2019-01-23 14:54:00'),
(12, 12, 1, 34000, 0, 34000, '2019-02-11 06:40:32', '2019-02-11 06:40:32'),
(13, 13, 2, 0, 34000, 34000, '2019-03-17 02:35:51', '2019-03-17 02:35:51'),
(14, 14, 2, 30000, 4000, 34000, '2019-03-17 02:36:50', '2019-03-17 02:36:50'),
(15, 15, 2, 17000, 0, 17000, '2019-03-17 02:37:17', '2019-03-17 02:37:17'),
(16, 16, 0, 34000, 0, 34000, '2019-03-17 02:41:11', '2019-03-17 02:41:11'),
(17, 17, 0, 32000, 2000, 34000, '2019-03-17 02:41:50', '2019-03-17 02:41:50'),
(18, 18, 0, 34000, 0, 34000, '2019-03-17 02:55:31', '2019-03-17 02:55:31'),
(19, 19, 0, 34000, 0, 34000, '2019-03-17 02:56:37', '2019-03-17 02:56:37'),
(20, 20, 0, 24000, 10000, 34000, '2019-03-17 02:57:25', '2019-03-17 02:57:25'),
(21, 21, 2, 32000, 2000, 34000, '2019-03-17 02:59:40', '2019-03-17 02:59:40'),
(22, 22, 1, 32000, 2000, 34000, '2019-03-17 03:01:28', '2019-03-17 03:01:28'),
(23, 23, 2, 14000, -20000, -6000, '2019-04-02 11:59:59', '2019-04-02 11:59:59'),
(24, 24, 2, 1400000, 0, 1400000, '2019-04-04 12:14:41', '2019-04-04 12:14:41'),
(25, 25, 2, 3400000, 0, 3400000, '2019-04-05 01:57:34', '2019-04-05 01:57:34'),
(26, 26, 2, 3400000, 0, 3400000, '2019-04-05 02:00:10', '2019-04-05 02:00:10'),
(27, 27, 2, 1700000, 0, 1700000, '2019-04-05 02:12:58', '2019-04-05 02:12:58'),
(28, 28, 2, 3400000, 0, 3400000, '2019-04-05 02:14:31', '2019-04-05 02:14:31'),
(29, 29, 2, 3400000, 0, 3400000, '2019-04-05 02:16:11', '2019-04-05 02:16:11'),
(30, 30, 2, 1700000, 0, 1700000, '2019-04-05 02:18:13', '2019-04-05 02:18:13'),
(31, 31, 2, 1700000, 0, 1700000, '2019-04-05 02:25:18', '2019-04-05 02:25:18'),
(32, 32, 2, 1700000, 0, 1700000, '2019-04-05 02:28:12', '2019-04-05 02:28:12'),
(33, 33, 2, 3400000, 0, 3400000, '2019-04-05 02:29:17', '2019-04-05 02:29:17'),
(34, 34, 2, 3400000, 0, 3400000, '2019-04-05 02:37:53', '2019-04-05 02:37:53'),
(35, 35, 1, 1700000, 1700000, 3400000, '2019-04-05 02:44:55', '2019-04-05 02:44:55'),
(36, 36, 2, 3400000, 0, 3400000, '2019-04-05 02:46:22', '2019-04-05 02:46:22'),
(37, 37, 2, 1700000, 0, 1700000, '2019-04-05 02:52:50', '2019-04-05 02:52:50'),
(38, 38, 1, 119818, 1580182, 1700000, '2019-04-05 02:53:57', '2019-04-05 02:53:57'),
(39, 39, 2, 1700000, 0, 1700000, '2019-04-05 02:58:53', '2019-04-05 02:58:53');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tokos`
--

CREATE TABLE `tokos` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama_toko` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_pemilik` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foto` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_telepon` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hari_buka` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hari_tutup` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jam_buka` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jam_tutup` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@gmail.com', NULL, '$2y$10$Y/uEy5dXf4O7biO1Knj4HOATw.Rrho3ETxHdaBeIsHne3gkEOzmL2', '799Gg793IXknvyfdRMiUp5fTgCz81P7Iw6od9nO9oHgNqM84FIN3RdW0Uucz', '2019-01-23 13:57:25', '2019-01-23 13:57:25'),
(2, 'karyawan', 'karyawan@gmail.com', NULL, '$2y$10$9qfM.Sj5xnCijAbHoYGZTuR/EqqNKtwq5JU9R9U7SUYDkNLD0lr0u', 'ePkBadig3KNJULrMKtH8jpseTA8SqWR9GYBEt3oms2rrQGUEmJrH5b8a7cSU', '2019-01-23 13:57:25', '2019-01-23 13:57:25'),
(3, 'ara222', 'admian@gmail.com', NULL, '$2y$10$bgUI/WRyOQgswv0MOxMUceqFEs0jdv5uEZzV.FoJcCTD.ocHTw5mK', NULL, '2019-04-03 01:30:34', '2019-04-03 01:30:34'),
(4, 'afiung', 'rafy1@gmail.com', NULL, '$2y$10$R9oA1sCFY/adKufcnRHZUOVEF8mcOdg31Nk4rxFscfRmdFradjuHq', NULL, '2019-04-04 03:15:59', '2019-04-04 03:15:59'),
(5, 'ara', 'ara2@gmail.com', NULL, '$2y$10$xgGqVEo/JezkZvSodTGgSuBeYjVBgpmXkV4VyCF7SQxGHaOUpe6fG', NULL, '2019-04-04 03:17:23', '2019-04-04 03:17:23');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `barangs`
--
ALTER TABLE `barangs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `barangs_satuan_id_foreign` (`satuan_id`),
  ADD KEY `barangs_merk_id_foreign` (`merk_id`),
  ADD KEY `barangs_kategori_id_foreign` (`kategori_id`),
  ADD KEY `barangs_supplier_id_foreign` (`supplier_id`);

--
-- Indeks untuk tabel `barang_keluars`
--
ALTER TABLE `barang_keluars`
  ADD PRIMARY KEY (`id`),
  ADD KEY `barang_keluars_barang_id_foreign` (`barang_id`),
  ADD KEY `barang_keluars_id_pembuat_foreign` (`id_pembuat`);

--
-- Indeks untuk tabel `barang_masuks`
--
ALTER TABLE `barang_masuks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `barang_masuks_barang_id_foreign` (`barang_id`),
  ADD KEY `barang_masuks_id_pembuat_foreign` (`id_pembuat`),
  ADD KEY `barang_masuks_id_supplier_foreign` (`id_supplier`);

--
-- Indeks untuk tabel `costumers`
--
ALTER TABLE `costumers`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `foto_barangs`
--
ALTER TABLE `foto_barangs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `foto_barangs_barang_id_foreign` (`barang_id`);

--
-- Indeks untuk tabel `info_tokos`
--
ALTER TABLE `info_tokos`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `kategoris`
--
ALTER TABLE `kategoris`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `laporan_pemasukans`
--
ALTER TABLE `laporan_pemasukans`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `laporan_pengeluarans`
--
ALTER TABLE `laporan_pengeluarans`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `merks`
--
ALTER TABLE `merks`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orders_id_pembuat_foreign` (`id_pembuat`),
  ADD KEY `orders_barang_id_foreign` (`barang_id`);

--
-- Indeks untuk tabel `order_details`
--
ALTER TABLE `order_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_details_order_id_foreign` (`order_id`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`);

--
-- Indeks untuk tabel `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Indeks untuk tabel `permission_user`
--
ALTER TABLE `permission_user`
  ADD PRIMARY KEY (`user_id`,`permission_id`,`user_type`),
  ADD KEY `permission_user_permission_id_foreign` (`permission_id`);

--
-- Indeks untuk tabel `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indeks untuk tabel `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`user_id`,`role_id`,`user_type`),
  ADD KEY `role_user_role_id_foreign` (`role_id`);

--
-- Indeks untuk tabel `satuans`
--
ALTER TABLE `satuans`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `suppliers`
--
ALTER TABLE `suppliers`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tagihans`
--
ALTER TABLE `tagihans`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tagihans_order_detail_id_foreign` (`order_detail_id`);

--
-- Indeks untuk tabel `tokos`
--
ALTER TABLE `tokos`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `barangs`
--
ALTER TABLE `barangs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `barang_keluars`
--
ALTER TABLE `barang_keluars`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `barang_masuks`
--
ALTER TABLE `barang_masuks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `costumers`
--
ALTER TABLE `costumers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `foto_barangs`
--
ALTER TABLE `foto_barangs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `info_tokos`
--
ALTER TABLE `info_tokos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `kategoris`
--
ALTER TABLE `kategoris`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `laporan_pemasukans`
--
ALTER TABLE `laporan_pemasukans`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `laporan_pengeluarans`
--
ALTER TABLE `laporan_pengeluarans`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `merks`
--
ALTER TABLE `merks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT untuk tabel `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT untuk tabel `order_details`
--
ALTER TABLE `order_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT untuk tabel `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `satuans`
--
ALTER TABLE `satuans`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `suppliers`
--
ALTER TABLE `suppliers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `tagihans`
--
ALTER TABLE `tagihans`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT untuk tabel `tokos`
--
ALTER TABLE `tokos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `barangs`
--
ALTER TABLE `barangs`
  ADD CONSTRAINT `barangs_kategori_id_foreign` FOREIGN KEY (`kategori_id`) REFERENCES `kategoris` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `barangs_merk_id_foreign` FOREIGN KEY (`merk_id`) REFERENCES `merks` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `barangs_satuan_id_foreign` FOREIGN KEY (`satuan_id`) REFERENCES `satuans` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `barangs_supplier_id_foreign` FOREIGN KEY (`supplier_id`) REFERENCES `suppliers` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `barang_keluars`
--
ALTER TABLE `barang_keluars`
  ADD CONSTRAINT `barang_keluars_barang_id_foreign` FOREIGN KEY (`barang_id`) REFERENCES `barangs` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `barang_keluars_id_pembuat_foreign` FOREIGN KEY (`id_pembuat`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `barang_masuks`
--
ALTER TABLE `barang_masuks`
  ADD CONSTRAINT `barang_masuks_barang_id_foreign` FOREIGN KEY (`barang_id`) REFERENCES `barangs` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `barang_masuks_id_pembuat_foreign` FOREIGN KEY (`id_pembuat`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `barang_masuks_id_supplier_foreign` FOREIGN KEY (`id_supplier`) REFERENCES `suppliers` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `foto_barangs`
--
ALTER TABLE `foto_barangs`
  ADD CONSTRAINT `foto_barangs_barang_id_foreign` FOREIGN KEY (`barang_id`) REFERENCES `barangs` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_barang_id_foreign` FOREIGN KEY (`barang_id`) REFERENCES `barangs` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `orders_id_pembuat_foreign` FOREIGN KEY (`id_pembuat`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `order_details`
--
ALTER TABLE `order_details`
  ADD CONSTRAINT `order_details_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `permission_user`
--
ALTER TABLE `permission_user`
  ADD CONSTRAINT `permission_user_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `tagihans`
--
ALTER TABLE `tagihans`
  ADD CONSTRAINT `tagihans_order_detail_id_foreign` FOREIGN KEY (`order_detail_id`) REFERENCES `order_details` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
