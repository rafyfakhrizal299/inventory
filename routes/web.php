<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
//kategori
Route::group(['prefix'=>'admin', 'middleware'=>['auth','role:Admin']], function () {
	
	Route::post('storekategori','KategoriController@store')->name('tambahkategori');
	Route::resource('/kategori','KategoriController');
	Route::get('ajaxdata/removedatakategori', 'KategoriController@removedata')->name('ajaxdata');
	Route::post('kategori/edit/{id}','KategoriController@update');
	Route::get('kategori/getedit/{id}','KategoriController@edit');
	//satuan
	
	Route::post('storesatuan','SatuanController@store')->name('tambah');
	Route::resource('/satuan','SatuanController');
	Route::get('ajaxdata/removedatasatuan', 'SatuanController@removedata');
	Route::post('satuan/edit/{id}','SatuanController@update');
	Route::get('satuan/getedit/{id}','SatuanController@edit');
	//supplier
	
	Route::post('storesupplier','SupplierController@store')->name('tambah');
	Route::resource('/supplier','SupplierController');
	Route::get('ajaxdata/removedatasupplier', 'SupplierController@removedata');
	Route::post('supplier/edit/{id}','SupplierController@update');
	Route::get('supplier/getedit/{id}','SupplierController@edit');
	Route::get('/supplier/downloadPDF/{view_type}','SupplierController@downloadPDF');
	//merk
	Route::post('storemerk','MerkBarangController@store')->name('tambah');
	Route::resource('/merk','MerkBarangController');
	Route::get('ajaxdata/removedatamerk', 'MerkBarangController@removedata');
	Route::post('merk/edit/{id}','MerkBarangController@update');
	Route::get('merk/getedit/{id}','MerkBarangController@edit');
	//barang
	
	Route::post('storebarang','BarangController@store')->name('tambah');
	Route::resource('/barang','BarangController');
	Route::get('ajaxdata/removedatabarang', 'BarangController@removedata');
	Route::post('barang/edit/{id}','BarangController@update');
	Route::get('barang/getedit/{id}','BarangController@edit');
	Route::get('/barang/downloadPDF/{view_type}','BarangController@downloadPDF');
	//barang masuk
	Route::post('storebarangmasuk','BarangMasukController@store')->name('tambah');
	Route::resource('/barangmasuk','BarangMasukController');
	Route::get('ajaxdata/removedatabarangmasuk', 'BarangMasukController@removedata');
	Route::post('barangmasuk/edit/{id}','BarangMasukController@update');
	Route::get('barangmasuk/getedit/{id}','BarangMasukController@edit');

	//laporan_pengeluaran
	Route::resource('laporan_pengeluaran','LaporanPengeluaran');
	Route::post('laporan_pengeluaran_total' , 'LaporanPengeluaran@index2');
	//laporan_pemasukan
	Route::resource('laporan_pemasukan','LaporanPemasukanController');
	Route::post('laporan_pemasukan_total' , 'LaporanPemasukanController@index2');
	//barang keluar
	Route::post('storebarangkeluar','BarangKeluarController@store')->name('tambah');
	Route::resource('/barangkeluar','BarangKeluarController');
	Route::get('ajaxdata/removedatabarangkeluar', 'BarangKeluarController@removedata');
	Route::post('barangkeluar/edit/{id}','BarangKeluarController@update');
	Route::get('barangkeluar/getedit/{id}','BarangKeluarController@edit');
	Route::get('/supplier/downloadPDF/{view_type}','SupplierController@downloadPDF');
	//order
	Route::post('storeorder','OrderController@store')->name('tambah');
	Route::resource('/order','OrderController');
	Route::get('ajaxdata/removedataorder', 'OrderController@removedata');
	Route::post('order/update/{kode_order}','OrderController@update');
	Route::get('order/edit/{kode_order}','OrderController@edit');
	Route::get('order/getedit/{id}','OrderController@edit');
	Route::get('order/show/{kode_order}','OrderController@show');
	Route::post('order/hapus','OrderController@deleteDataSebagian')->name('deleteBarang.beberapa');
	//barang_keluar	
	Route::resource('/barangkeluar','BarangKeluarController');
	Route::get('ajaxdata/removedatabarangkeluar', 'BarangKeluarController@removedata');
	Route::post('barangkeluar/edit/{id}','BarangKeluarController@update');
	Route::get('barangkeluar/getedit/{id}','BarangKeluarController@edit');
	Route::get('/barangkeluar/downloadPDF/{view_type}','BarangKeluarController@downloadPDF');

	//tagihan
	Route::resource('/tagihan','TagihanController');
	Route::get('ajaxdata/removedatatagihan', 'TagihanController@removedata');
	Route::post('tagihan/edit/{id}','TagihanController@update');
	Route::get('tagihan/getedit/{id}','TagihanController@edit');
	//user
	Route::post('storeuser','UserController@store')->name('tambah');
	Route::resource('/user','UserController');
	Route::get('ajaxdata/removedatauser', 'UserController@removedata');
	Route::post('user/edit/{id}','UserController@update');
	Route::get('user/getedit/{id}','UserController@edit');
	//print
	Route::resource('order_detail','DetailOrderController');
	// Route::get('/showData',array('as'=>'showData','uses'=>'DetailOrderController@showData'));
	Route::get('/printNota',array('as'=>'printNota','uses'=>'DetailOrderController@printInvoice'));
});

Route::group(['prefix'=>'karyawan', 'middleware'=>['auth','role:Admin|Karyawan']], function () {
	//barang_masuk
	Route::post('storebarangmasuk','BarangMasukController@store')->name('tambah');
	Route::resource('barangmasuk','BarangMasukController');
	Route::get('ajaxdata/removedatabarangmasuk', 'BarangMasukController@removedata');
	Route::post('barangmasuk/edit/{id}','BarangMasukController@update');
	Route::get('barangmasuk/getedit/{id}','BarangMasukController@edit');
		//order
	Route::post('storeorder','OrderController@store')->name('tambah');
	Route::resource('order','OrderController');
	Route::get('ajaxdata/removedataorder', 'OrderController@removedata');
	Route::post('order/edit/{id}','OrderController@update');
	Route::get('order/getedit/{id}','OrderController@edit');
	//barang_keluar
	Route::resource('barangkeluar','BarangKeluarController');
	Route::get('ajaxdata/removedatabarangkeluar', 'BarangKeluarController@removedata');
	Route::post('barangkeluar/edit/{id}','BarangKeluarController@update');
	Route::get('barangkeluar/getedit/{id}','BarangKeluarController@edit');
	//tagihan
	Route::resource('tagihan','TagihanController');
	Route::get('ajaxdata/removedatatagihan', 'TagihanController@removedata');
	Route::post('tagihan/edit/{id}','TagihanController@update');
	Route::get('tagihan/getedit/{id}','TagihanController@edit');

	Route::resource('detail_order','DetailOrderController');
	Route::get('/printNota',array('as'=>'printNota','uses'=>'DetailOrderController@printInvoice'));

});
Route::get('/jsonuser','UserController@jsonuser');
Route::get('/jsonkategori','KategoriController@jsonkategori');
Route::get('/jsonsatuan','SatuanController@jsonsatuan');
Route::get('/jsonsupplier','SupplierController@jsonsupplier');
Route::get('/jsonmerk','MerkBarangController@jsonmerk');
Route::get('/jsonbarang','BarangController@jsonbarang');
Route::get('/jsonbarangmasuk','BarangMasukController@jsonbarangmasuk');
Route::get('/jsonbarangkeluar','BarangKeluarController@jsonbarangkeluar');
Route::get('/jsonorder','OrderController@jsonorder');
Route::get('/jsonbarangkeluar','BarangKeluarController@jsonbarangkeluar');
Route::get('/jsontagihan','TagihanController@jsontagihan');
Route::get('/jsonorderdetail','DetailOrderController@jsonorderdetail');

Route::get('/kode_order',array('as'=>'kode_order','uses'=>'OrderController@kode_order'));

Route::get('/findprice',array('as'=>'findPrice','uses'=>'OrderController@findPrice'));
Route::get('/findSupplier',array('as'=>'findSupplier','uses'=>'BarangMasukController@findSupplier'));

Route::get('barang/exportExcelBarang',[
    	'as'=> 'export.barang',
    	'uses' => 'BarangController@downloadExcel'
]);
Route::get('supplier/exportExcelSupplier',[
    	'as'=> 'export.supplier',
    	'uses' => 'SupplierController@downloadExcel'
]);
Route::get('barangkeluar/exportExcelSupplier',[
    	'as'=> 'export.barangkeluar',
    	'uses' => 'BarangKeluarController@downloadExcel'
]);


