$(document).ready(function() {
      createData();
      $('#dataTableOrder').DataTable({
            processing: true,
            serverSide: true,
            ajax: '/jsonorder',
            columns:[
                  { data: 'select', orderable: false, searchable: false},
                  { data: 'kode_order', name: 'kode_order' },
                  { data: 'id_order'},
                  { data: 'id_order2'},
                  { data: 'action', orderable: false, searchable: false }
              ],
            });
    
        
      function createData() {
            $('#formOrder').submit(function(e){
                $.ajaxSetup({
                  header: {
                    'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
                  }
                });
                e.preventDefault();
                $.ajax({
                    url:'storeorder',
                    type:'post',
                    data: new FormData(this),
                    cache: true,
                    contentType: false,
                    processData: false,
                    async:false,
                    dataType: 'json',
                    success: function (data){
                      console.log(data);
                      // swal({
                      //     title:'Success Tambah!',
                      //     text: data.message,
                      //     type:'success',
                      //     timer:'2000'
                      //   });
                      var success = "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button>"
                      success += "Data Berhasil disimpan!"
                      $('#success').addClass('alert alert-success alert-dismissible fade in').html(success);
                      //
                      $('#success').attr('hidden',false);
                      $('#myModalBarangMasuk').modal('hide');
                      setTimeout(function(){
                        $('#success').fadeOut('slow');
                      }, 5000);
                      $('#TambahKeluar').attr('hidden',true);
                      $('#dataTableOrder').DataTable().ajax.reload();
                      $('.barangkeluar').attr('hidden',false);
                      state = "success"
                    },
                    error: function (data){
                      $('input').on('keydown keypress keyup click change', function(){
                      $(this).parent().removeClass('has-error');
                      $(this).next('.help-block').hide()
                    });
                      var coba = new Array();
                      console.log(data.responseJSON.errors);
                      $.each(data.responseJSON.errors,function(name, value){
                        console.log(name);
                        coba.push(name);
                        $('input[name='+name+']').parent().addClass('has-error');
                        $('input[name='+name+']').next('.help-block').show().text(value);
                      });
                      var errors = "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button>"
                      $.each(data.responseJSON.errors,function(name, value){
                        errors += "<strong>"+value+"</strong><br>"
                      })
                      $('#validation').addClass('alert alert-danger alert-dismissible fade in').html(errors);
                      $('input[name='+coba[0]+']').focus();
                      $('#validation').focus();
                      state = "errors"
                },
                    complete: function() {
                        $("#formOrder")[0].reset();
                        if (state == 'success') {
                          $('.barangKeluar').attr('hidden', false);
                          $('#validation').attr('hidden',true);
                        }
                        else{
                          $('.barangKeluar').attr('hidden', true)
                        }
                        $('#success').attr('hidden',false);
                    }
                });
          });
      }
      $(document).on('click', '.bulk_delete_barang' ,function(){
            var $check =[];
            var $check = $(".checkitem:checked").map(function(){
                return $(this).val();
            }).get();
            console.log($check);
          if(confirm("Apakah anda yakin ingin menghapus data ini?"))
          {
              if($check.length > 0)
              {
                $.ajaxSetup({
                   headersHee: {
                    'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
                  }
                });
                  $.ajax({
                      url:'order/hapus',
                      method:'post',
                      data:{
                          check:$check
                          },
                      complete: function() {
                        
                    }
                  });
              }else

              {
                  alert("Pilih satu data")
              }
          }
        });
      // $(document).on('click', '.editBarang', function(){
      //       var nomor = $(this).data('id');
      //       $('#formBarang').submit('');
      //       $.ajax({
      //         url:'barang/getedit' + '/' + nomor,
      //         method:'get',
      //         data:{id:nomor},
      //         dataType:'json',
      //         success:function(data){
      //           console.log(data);
      //           state = "update";
      //           $('#id').val(data.id);
      //           $('#nama_barang').val(data.nama_barang);
      //           $('#jenis_ukuran').val(data.jenis_ukuran);
      //           $('#asal').val(data.asal);
      //           $('#harga_beli').val(data.harga_beli);
      //           $('#harga_jual').val(data.harga_jual);
      //           $('#stok').val(data.stok);
      //           $('#satuan_id').val(data.satuan_id);
      //           $('#merk_id').val(data.merk_id);
      //           $('#kategori_id').val(data.kategori_id);
      //           $('#supplier_id').val(data.supplier_id);
      //           $('#myModalBarang').modal('show');
      //           $('#aksiBarang').val('Simpan');
      //           $('.modal-title').text('Masukan Data Barang');
      //           }
      //         });
      //     });
      $(document).on('click', '.showData', function(){
            var nomor = $(this).attr("id");
            $.ajax({
              url:'order/show' + '/' + nomor,
              method:'get',
              data:{id:nomor},
              dataType:'json',
              success:function(data){
                console.log(data);
                state = "update";

                var table_header = "<table class='table '><tr ><td>Nama Barang</td><td>Quantity</td><td>Harga</td><td>Jumlah</td></tr><tbody style='background-color='black'>";
                   var table_footer = "";
                   var kode_order = "";
                   var kodeorderjudul = "";
                   var html ="";
                   let satu = 1;


      data.forEach((elementby ,index ,array) => {
            if (index === (array.length - 1)){ 
            kode_order += "<h6><b>Tanggal Beli :  </b>"+elementby.order.tanggal_beli+"</h6>"
            kode_order += "<h6><b>Nama Pembeli : </b>"+elementby.order.nama_pembeli+"</h6>"
            kode_order += "<h6><b>Alamat : </b>"+elementby.order.alamat+"</h6>"
            kode_order += "<h6><b>No Telepon :  </b>"+elementby.order.no_telepon+"</h6>"
            kodeorderjudul += "<h2> Kode Order : "+elementby.kode_order+"</h2>"
        
           }
            });
              data.forEach((elementby ,index ,array) => {
                 if (index === (array.length - 1)){ 
                  table_footer += "</tbody><tfoot><tr><td style='border-bottom:none;'></td><td class='text-right'><h6>Sub Total</h6></td><td colspan='2' class='text-center'><h6> Rp."+elementby.sub_total+"</h6></td><tr><td style='border:none;'></td><td class='text-right'><h6>Potongan Harga</h6></td><td colspan='2' class='text-center'><h6> Rp."+elementby.potongan_harga+"</h6></td></tr><tr><td style='border:none;'></td><td class='text-right'><h4><b>Total</b></h4></td><td colspan='2' class='text-center'><h4><b> Rp."+elementby.total+"</b></h4></td></tr></tfoot></table>"
           } 
            });
               data.forEach(function(element) {
              html += "<tr><td>"+element.order.barang.nama_barang +"</td><td>"+element.order.quantity+" "+element.order.barang.satuan.nama_satuan+"</td><td>"+element.order.harga +"</td><td>"+element.order.jumlah+"</td></tr>";
            });
                var all = table_header +html+ table_footer;
                var kode_order = kode_order;
                $('#judul').html(kodeorderjudul);
                
                $('#kodeorderjudul').html(kodeorderjudul);
                $('#showkodeorder').html(kode_order);
                $('#listPembelian').html(all);
                console.log(kode_order);
                $('.judul-kode-order').html(data.kode_order);
                $('.sub_total1').html(data.sub_total);
                $('.sub_total2').html(data.total);
                $('#myModalShow').modal('show');                }                

              });
          });

    //     $('tr.click-tr').on('click',function() {
    //     $(this).addClass("selected").siblings().removeClass("selected");
    //     $('.barangKeluar').attr('hidden', true);
    //     $('#space').attr('hidden',true);
    //     $('#hidden-footer').removeAttr("style");
    //     $('#col1').addClass("col1"); 
    //     $('#TambahKeluar').removeAttr("style"); 
    //     $('#TambahKeluar').attr('hidden',false);
    //     $('.barangKeluar').attr('hidden', true);
      // $('tr.click-tr > td.del').attr('hidden',true);
      // $('thead').attr('hidden',true);
      // $('.td1').removeAttr('style'); 
      // $('.td1').attr('hidden',false); 

        
                   
    // });

   
    // $('#TambahKeluar').attr('hidden',true);
    //     $('#EditOrder').on('click',function() {
    //     $('.barangKeluar').attr('hidden', true);
    //     $('#space').attr('hidden',true);
    //        state = "insert";
    //     $('#hidden-footer').removeAttr("style");
    //     $('#col1').addClass("col1");  
    //     $('#TambahKeluar').removeAttr("style"); 
    //     $('#TambahKeluar').attr('hidden',false);
    //     $('tr.click-tr > td.del').attr('hidden',false);
    //   $('thead').attr('hidden',false);
    //   $('.td1').attr('hidden',true); 

      

    // });    

    // $('#TutupTambahKeluar,#TutupTambahKeluar2').on('click',function(){
    //     $('#TambahKeluar').attr('hidden',true);
    //     $('#col1').removeClass("col1");
    //     $('.barangKeluar').attr('hidden',false);
    //     $('#table2').attr('hidden', true);
    //     $('tr.click-tr > td.del').attr('hidden',false);
    //   $('thead').attr('hidden',false);
    //   $('.td1').attr('hidden',true); 
    // $('#col1').attr('hidden',false);
    //     $('#showOrder').attr('hidden',true);
        
      
    //     $('#table1').attr('hidden', false);



    // });
    $(document).on('click', '.editOrder', function(){
            $('#TambahKeluar').attr('hidden',false);
            $('.barangKeluar').attr('hidden', true);
            var nomor = $(this).attr("id");
            $('#formOrder').submit('');
            $.ajax({
              url:'order/edit' + '/' + nomor,
              method:'get',
              data:{id:nomor},
              dataType:'json',
              success:function(data){
                console.log(data);
                state = "update";
                var table_header = "<table class='table table-striped table-bordered responsive' id='table_hidden'><thead><tr><th>Nama Barang</th><th>Kuantitas</th><th>Harga Jual</th><th>Total</th></tr></thead>";
                var table_footer = "";
                var kode_order = "";
                var kodeorderjudul = "";
                var id = "";
                var nama_pembeli = "";
                var no_telepon = "";
                var alamat = "";
                var tanggal_beli = "";
                var subtotal = "";
                var total = "";
                var potongan_harga = "";
                var uang_muka = "";
                var html = "";
                var html2 = "";

                data.forEach((elementby, index,array)=>{
                  if (index === (array.length - 1)){
                    kodeorderjudul += "<h2>"+elementby.kode_order+"</h2>"
                    id += ""+elementby.id+""
                    nama_pembeli += ""+elementby.order.nama_pembeli+""
                    no_telepon += ""+elementby.order.no_telepon+""
                    alamat += ""+elementby.order.alamat+""
                    tanggal_beli += ""+elementby.order.tanggal_beli+""
                    kode_order += ""+elementby.kode_order+""
                    subtotal += ""+elementby.sub_total+""
                    total += ""+elementby.total+""
                    potongan_harga += ""+elementby.potongan_harga+""
                    uang_muka += ""+elementby.uang_muka+""
                  }
                });
                // var countone = 1;
                // data.forEach((element ,index ,array) => {
                //  if (index === (array.length - 1)){ 
                //     html += "<tbody id='coba'><tr id='row"+countone+"'><td><select name='barang_id[]' class='form-control' id='barang_id'><option>"+element.order.barang.nama_barang+"</option></select></td>'";
                //     html += "<td><input type='number' min='0' name='kuantitas[]' id='kuantitas' class='form-control kuantitas' value='"+element.order.quantity+"' /></td>";
                //     html += "<td><input type='number' name='harga_jual[]' id='harga_jual' class='form-control harga_jual' value='"+element.order.barang.harga_jual+"' readonly/></td>";
                //     html += "<td><input type='text' name='jumlah' id='jumlah' class='form-control jumlah' placeholder='Jumlah' value='"+element.order.jumlah+"' readonly/></td>";
                //     html += "<td><button type='button' class='btn btn-danger btn-sm remove' name='remove' data-row='row'"+ countone +"'><i class='fa fa-minus-square'></i></button></td>";
                //     html += "</tr>";
                //     html += "</tbody>";
                //   } 
                // });
                var count = 1;
                data.forEach(function(element) {
                  var counttotal = count ++;
                  console.log(counttotal)
                    html2 += "<tbody id='coba'><tr id='row"+counttotal+"'><td><select name='barang_id[]' class='form-control' id='barang_id'><option>"+element.order.barang.nama_barang+"</option></select></td>'";
                    html2 += "<td><input type='number' min='0' name='kuantitas[]' id='kuantitas' class='form-control kuantitas' value='"+element.order.quantity+"' /></td>";
                    html2 += "<td><input type='number' name='harga_jual[]' id='harga_jual' class='form-control harga_jual' value='"+element.order.barang.harga_jual+"' readonly/></td>";
                    html2 += "<td><input type='text' name='jumlah' id='jumlah' class='form-control jumlah' placeholder='Jumlah' value='"+element.order.jumlah+"' readonly/></td>";
                    html2 += "<td><button type='button' class='btn btn-danger btn-sm remove' name='remove' data-row='row'"+ counttotal +"'><i class='fa fa-minus-square'></i></button></td>";
                    html2 += "</tr>";
                    html2 += "</tbody>";
                });

                var all =table_header+html+html2;
                $('#nama_pembeli').val(nama_pembeli);
                $('#editTable').html(all);
                $('#alamat').val(alamat);
                $('#tanggal_beli').val(tanggal_beli);
                $('#kode_order').val(kode_order);
                $('#no_telepon').val(no_telepon);
                $('.subtotal').val(subtotal);
                $('.totalpotong').val(total);
                $('#potongan_harga').val(potongan_harga);
                $('.uang_muka').val(uang_muka);
                $('#editTable').val(all);
                $('#table_hidden').attr('hidden',true); 
                $('#editTable').attr('hidden',false); 
                
             }
           });
        });

      //dis
      $(document).on('click', '#closeStok', function(){
          $('#stokmin').hide();              
        });
      $(document).on('click', '#closeStokM', function(){
          $('#stokmink').hide();
              
        });


      $(document).on('click', '.editOrder', function(){
          $('.dis').prop('hidden',true);
          $('.dis').prop('disabled',true);
          
              
        });

      $(document).on('click', '.editOrder', function(){
                $('.non_dis').prop('hidden',false);
                $('.non_dis').prop('disabled',false);
                
                    
              });

      $(document).on('click', '#TambahOrder', function(){
                $('.non_dis').prop('hidden',true);
                $('.non_dis').prop('disabled',true);
                
                    
              });

      $(document).on('click', '#TambahOrder', function(){
                $('.dis').prop('hidden',false);
                $('.dis').prop('disabled',false);
                
                    
              });

      $(document).on('click', '#add', function(){
                $('.non_dis').prop('hidden',true);
                $('.non_dis').prop('disabled',true);
                
                    
              });
      });

