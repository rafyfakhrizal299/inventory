  $(document).ready(function() {
    createData();
          $('#dataTableBarang').DataTable({
            processing: true,
            serverSide: true,
            ajax: '/jsonbarang',
            columns:[
                  { data: 'nama_barang', name: 'nama_barang' },
                  { data: 'jenis_asal', name: 'jenis_asal' },
                  { data: 'harga_beli'},
                  { data: 'harga_jual'},
                  { data: 'stok'},
                  { data: 'merk_id'}, 
                  { data: 'kategori_id'},
                  { data: 'supplier_id' },
                  { data: 'action', orderable: false, searchable: false }
              ],
            });

          $('#TambahBarang').click(function(){
            $('#myModalBarang').modal('show');
            $('.modal-title').text('Masukan Data Barang');
            // $('.select-pilih').select2();
            $('#aksiBarang').val('TambahBarang');
            state = "insert";
            });

           $('#myModalBarang').on('hidden.bs.modal',function(e){
            $(this).find('#formBarang')[0].reset();
            $('span.has-error').text('');
            $('.form-group.has-error').removeClass('has-error');
            });

        //      $('#formBarang').submit(function(e){
           //      $.ajaxSetup({
           //        header: {
           //          'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
           //        }
           //      });
        //     error.preventDefault();

        //     if (state == 'insert'){
              
        //       $.ajax({
        //         type: "POST",
        //         url: "{{url ('/storeBarang')}}",
        //         data: new FormData(this),
        //        // data: $('#student_form').serialize(),
        //         contentType: false,
        //         processData: false,
        //         dataType: 'json',
        //         success: function (data){
        //           console.log(data);
        //           swal({
        //               title:'Success Tambah!',
        //               text: data.message,
        //               type:'success',
        //               timer:'2000'
        //             });
        //           $('#myModalBarang').modal('hide');
        //           $('#dataTableBarang').DataTable().ajax.reload();
        //         },
        //         //menampilkan validasi error
        //         error: function (data){
        //           $('input').on('keydown keypress keyup click change', function(){
        //           $(this).parent().removeClass('has-error');
        //           $(this).next('.help-block').hide()
        //         });
        //           var coba = new Array();
        //           console.log(data.responseJSON.errors);
        //           $.each(data.responseJSON.errors,function(name, value){
        //             console.log(name);
        //             coba.push(name);
        //             $('input[name='+name+']').parent().addClass('has-error');
        //             $('input[name='+name+']').next('.help-block').show().text(value);
        //           });
        //           $('input[name='+coba[0]+']').focus();
        //         }
        //       });
        //     }
        // });

        function createData() {
            $('#formBarang').submit(function(e){
                $.ajaxSetup({
                  header: {
                    'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
                  }
                });
                e.preventDefault();
                if (state == 'insert') {
                $.ajax({
                    url:'storebarang',
                    type:'post',
                    data: new FormData(this),
                    cache: true,
                    contentType: false,
                    processData: false,
                    async:false,
                    dataType: 'json',
                    success: function (data){
                      console.log(data);
                      // swal({
                      //     title:'Success Tambah!',
                      //     text: data.message,
                      //     type:'success',
                      //     timer:'2000'
                      //   });
                      var success = "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button>"
                      success += "Data Berhasil disimpan!"
                      $('#success').addClass('alert alert-success alert-dismissible fade in').html(success);
                      //
                      $('#success').attr('hidden',false);
                      setTimeout(function(){
                        $('#success').fadeOut('slow');
                      }, 5000);
                      $('#myModalBarang').modal('hide');
                      $('#dataTableBarang').DataTable().ajax.reload();
                    },
                    error: function (data){
                      $('input').on('keydown keypress keyup click change', function(){
                      $(this).parent().removeClass('has-error');
                      $(this).next('.help-block').hide()
                    });
                      var coba = new Array();
                      console.log(data.responseJSON.errors);
                      $.each(data.responseJSON.errors,function(name, value){
                        console.log(name);
                        coba.push(name);
                        $('input[name='+name+']').parent().addClass('has-error');
                        $('input[name='+name+']').next('.help-block').show().text(value);
                      });
                      $('input[name='+coba[0]+']').focus();
                },
                    complete: function() {
                        $("#formBarang")[0].reset();
                    }
                                   
              });
            }
            else{
                $.ajax({
                    url:'barang/edit' + '/' + $('#id').val(),
                    type:'post',
                    data: new FormData(this),
                    cache: true,
                    contentType: false,
                    processData: false,
                    async:false,
                    dataType: 'json',
                    success: function (data){
                      console.log(data);
                      swal({
                          title:'Success Edit !',
                          text: data.message,
                          type:'success',
                          timer:'2000'
                        });
                      $('#myModalBarang').modal('hide');
                      $('#dataTableBarang').DataTable().ajax.reload();
                    },
                    complete: function() {
                        $("#formBarang")[0].reset();
                    }
                });

            }
            });
        }
        $(document).on('click', '.editBarang', function(){
            var nomor = $(this).data('id');
            $('#formBarang').submit('');
            $.ajax({
              url:'barang/getedit' + '/' + nomor,
              method:'get',
              data:{id:nomor},
              dataType:'json',
              success:function(data){
                console.log(data);
                state = "update";
                $('#id').val(data.id);
                $('#nama_barang').val(data.nama_barang);
                $('#jenis_ukuran').val(data.jenis_ukuran);
                $('#asal').val(data.asal);
                $('#harga_beli').val(data.harga_beli);
                $('#harga_jual').val(data.harga_jual);
                $('#stok').val(data.stok);
                $('#satuan_id').val(data.satuan_id);
                $('#merk_id').val(data.merk_id);
                $('#kategori_id').val(data.kategori_id);
                $('#supplier_id').val(data.supplier_id);
                $('#myModalBarang').modal('show');
                $('#aksiBarang').val('Simpan');
                $('.modal-title').text('Masukan Data Barang');
                }
              });
          });

        $(document).on('hide.bs.modal','#myModalBarang', function() {
                $('#dataTableBarang').DataTable().ajax.reload();
              });
        $(document).on('click', '.deleteBarang', function(){
                var bebas = $(this).attr('id');
                  if (confirm("Yakin Dihapus ?")) {
                    $.ajax({
                      url: 'ajaxdata/removedatabarang',
                      method: "get",
                      data:{id:bebas},
                      success: function(data){
                        swal({
                          title:'Success Delete!',
                          text:'Data Berhasil Dihapus',
                          type:'success',
                          timer:'1500'
                        });
                        $('#dataTableBarang').DataTable().ajax.reload();
                      }
                    })
                  }
                  else
                  {
                    swal({
                      title:'Batal',
                      text:'Data Tidak Jadi Dihapus',
                      type:'error',
                      });
                    return false;
                  }
                });
        
});