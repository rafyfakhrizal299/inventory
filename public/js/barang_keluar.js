  $(document).ready(function() {
    createData();
          $('#dataTableBarangKeluar').DataTable({
            processing: true,
            serverSide: true,
            ajax: '/jsonbarangkeluar',
            columns:[
                  { data: 'barang_id'},
                  { data: 'id_pembuat'},
                  { data: 'nama_pembeli', name: 'nama_pembeli' },
                  { data: 'tanggal_beli', name: 'tanggal_beli' },
                  { data: 'quantity', name: 'quantity' },
                  { data: 'harga', name: 'harga' },
                  { data: 'jumlah', name: 'jumlah' },
                  { data: 'action', orderable: false, searchable: false }
              ],
            });

          $('#TambahBarangKeluar').click(function(){
            $('#myModalBarangKeluar').modal('show');
            $('.modal-title').text('Keluaran Data Barang Keluar');
            $('.select-pilih').select2();
            $('#aksiBarangKeluar').val('TambahBarangKeluar');
            state = "insert";
            });

           $('#myModalBarangKeluar').on('hidden.bs.modal',function(e){
            $(this).find('#formBarangKeluar')[0].reset();
            $('span.has-error').text('');
            $('.form-group.has-error').removeClass('has-error');
            });

        //      $('#formBarangKeluar').submit(function(e){
           //      $.ajaxSetup({
           //        header: {
           //          'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
           //        }
           //      });
        //     error.preventDefault();

        //     if (state == 'insert'){
              
        //       $.ajax({
        //         type: "POST",
        //         url: "{{url ('/storeBarangKeluar')}}",
        //         data: new FormData(this),
        //        // data: $('#student_form').serialize(),
        //         contentType: false,
        //         processData: false,
        //         dataType: 'json',
        //         success: function (data){
        //           console.log(data);
        //           swal({
        //               title:'Success Tambah!',
        //               text: data.message,
        //               type:'success',
        //               timer:'2000'
        //             });
        //           $('#myModalBarangKeluar').modal('hide');
        //           $('#dataTableBarangKeluar').DataTable().ajax.reload();
        //         },
        //         //menampilkan validasi error
        //         error: function (data){
        //           $('input').on('keydown keypress keyup click change', function(){
        //           $(this).parent().removeClass('has-error');
        //           $(this).next('.help-block').hide()
        //         });
        //           var coba = new Array();
        //           console.log(data.responseJSON.errors);
        //           $.each(data.responseJSON.errors,function(name, value){
        //             console.log(name);
        //             coba.push(name);
        //             $('input[name='+name+']').parent().addClass('has-error');
        //             $('input[name='+name+']').next('.help-block').show().text(value);
        //           });
        //           $('input[name='+coba[0]+']').focus();
        //         }
        //       });
        //     }
        // });

        function createData() {
            $('#formBarangKeluar').submit(function(e){
                $.ajaxSetup({
                  header: {
                    'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
                  }
                });
                e.preventDefault();
                $.ajax({
                    url:'barangkeluar/edit' + '/' + $('#id').val(),
                    type:'post',
                    data: new FormData(this),
                    cache: true,
                    contentType: false,
                    processData: false,
                    async:false,
                    dataType: 'json',
                    success: function (data){
                      console.log(data);
                      swal({
                          title:'Success Edit !',
                          text: data.message,
                          type:'success',
                          timer:'2000'
                        });
                      $('#myModalBarangKeluar').modal('hide');
                      $('#dataTableBarangKeluar').DataTable().ajax.reload();
                    },
                    complete: function() {
                        $("#formBarangKeluar")[0].reset();
                    }
                });
            });
        }
        $(document).on('click', '.editBarangKeluar', function(){
            var nomor = $(this).data('id');
            $('#formBarangKeluar').submit('');
            $.ajax({
              url:'barangkeluar/getedit' + '/' + nomor,
              method:'get',
              data:{id:nomor},
              dataType:'json',
              success:function(data){
                console.log(data);
                state = "update";
                $('#id').val(data.id);
                $('#kuantitas').val(data.quantity);
                $('#harga').val(data.harga);
                $('#myModalBarangKeluar').modal('show');
                $('#aksiBarangKeluar').val('Simpan');
                $('.modal-title').text('Keluaran Data Barang Keluar');
                }
              });
          });

        $(document).on('hide.bs.modal','#myModalBarangKeluar', function() {
                $('#dataTableBarangKeluar').DataTable().ajax.reload();
              });
        $(document).on('click', '.deleteBarangKeluar', function(){
                var bebas = $(this).attr('id');
                  if (confirm("Yakin Dihapus ?")) {
                    $.ajax({
                      url: 'ajaxdata/removedatabarangkeluar',
                      method: "get",
                      data:{id:bebas},
                      success: function(data){
                        swal({
                          title:'Success Delete!',
                          text:'Data Berhasil Dihapus',
                          type:'success',
                          timer:'1500'
                        });
                        $('#dataTableBarangKeluar').DataTable().ajax.reload();
                      }
                    })
                  }
                  else
                  {
                    swal({
                      title:'Batal',
                      text:'Data Tidak Jadi Dihapus',
                      type:'error',
                      });
                    return false;
                  }
                });
        
});