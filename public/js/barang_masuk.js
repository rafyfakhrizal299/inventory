  $(document).ready(function() {
    createData();
          $('#dataTableBarangMasuk').DataTable({
            processing: true,
            serverSide: true,
            ajax: '/jsonbarangmasuk',
            columns:[
                  { data: 'barang_id'},
                  { data: 'stok_masuk'},
                  { data: 'harga_beli'},
                  { data: 'id_supplier'},
                  { data: 'tanggal_masuk'},
                  { data: 'total'},
                  { data: 'action', orderable: false, searchable: false }
              ],
            });

          $('#TambahMasuk').on('click',function(){
            $('#bm_edit').detach();
            $('#bm_create').attr('hidden',false);
            state = "insert"; 
            });

           $('#myModalBarangMasuk').on('hidden.bs.modal',function(e){
            $(this).find('#formBarangMasuk')[0].reset();
            $('span.has-error').text('');
            $('.form-group.has-error').removeClass('has-error');
            });

        //      $('#formBarangMasuk').submit(function(e){
           //      $.ajaxSetup({
           //        header: {
           //          'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
           //        }
           //      });
        //     error.preventDefault();

        //     if (state == 'insert'){
              
        //       $.ajax({
        //         type: "POST",
        //         url: "{{url ('/storeBarangMasuk')}}",
        //         data: new FormData(this),
        //        // data: $('#student_form').serialize(),
        //         contentType: false,
        //         processData: false,
        //         dataType: 'json',
        //         success: function (data){
        //           console.log(data);
        //           swal({
        //               title:'Success Tambah!',
        //               text: data.message,
        //               type:'success',
        //               timer:'2000'
        //             });
        //           $('#myModalBarangMasuk').modal('hide');
        //           $('#dataTableBarangMasuk').DataTable().ajax.reload();
        //         },
        //         //menampilkan validasi error
        //         error: function (data){
        //           $('input').on('keydown keypress keyup click change', function(){
        //           $(this).parent().removeClass('has-error');
        //           $(this).next('.help-block').hide()
        //         });
        //           var coba = new Array();
        //           console.log(data.responseJSON.errors);
        //           $.each(data.responseJSON.errors,function(name, value){
        //             console.log(name);
        //             coba.push(name);
        //             $('input[name='+name+']').parent().addClass('has-error');
        //             $('input[name='+name+']').next('.help-block').show().text(value);
        //           });
        //           $('input[name='+coba[0]+']').focus();
        //         }
        //       });
        //     }
        // });

        function createData() {
            $('#formBarangMasuk').submit(function(e){
                $.ajaxSetup({
                  header: {
                    'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
                  }
                });
                e.preventDefault();
                if (state == 'insert') {
                $.ajax({
                    url:'storebarangmasuk',
                    type:'post',
                    data: new FormData(this),
                    cache: true,
                    contentType: false,
                    processData: false,
                    async:false,
                    dataType: 'json',
                    success: function (data){
                      console.log(data);
                      var success = "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button>"
                      success += "Data Berhasil disimpan!"
                      $('#success').addClass('alert alert-success alert-dismissible fade in').html(success);
                      //
                      $('#success').attr('hidden',false);
                      setTimeout(function(){
                        $('#success').fadeOut('slow');
                      }, 5000);
                      $('#myModalBarangMasuk').modal('hide');
                      $('#TambahMasuk').attr('hidden',true);
                      $('#dataTableBarangMasuk').DataTable().ajax.reload();
                    },
                    error: function (data){
                      $('input').on('keydown keypress keyup click change', function(){
                      $(this).parent().removeClass('has-error');
                      $(this).next('.help-block').hide()
                    });
                      var coba = new Array();
                      console.log(data.responseJSON.errors);
                      $.each(data.responseJSON.errors,function(name, value){
                        console.log(name);
                        coba.push(name);
                        $('input[name='+name+']').parent().addClass('has-error');
                        $('input[name='+name+']').next('.help-block').show().text(value);
                      });
                      $('input[name='+coba[0]+']').focus();
                },
                    complete: function() {
                        $("#formBarangMasuk")[0].reset();
                        $('.barangMasuk').attr('hidden', false);

                    }
                });
              }
              else{
                  $.ajax({
                      url:'barangmasuk/edit' + '/' + $('#id').val(),
                      type:'post',
                      data: new FormData(this),
                      cache: true,
                      contentType: false,
                      processData: false,
                      async:false,
                      dataType: 'json',
                      success: function (data){
                        console.log(data);
                        swal({
                            title:'Success Edit !',
                            text: data.message,
                            type:'success',
                            timer:'2000'
                          });
                        $('#myModalBarangMasuk').modal('hide');
                        $('#dataTableBarangMasuk').DataTable().ajax.reload();
                      },
                      error: function (data){
                      $('input').on('keydown keypress keyup click change', function(){
                      $(this).parent().removeClass('has-error');
                      $(this).next('.help-block').hide()
                    });
                      var coba = new Array();
                      console.log(data.responseJSON.errors);
                      $.each(data.responseJSON.errors,function(name, value){
                        console.log(name);
                        coba.push(name);
                        $('input[name='+name+']').parent().addClass('has-error');
                        $('input[name='+name+']').next('.help-block').show().text(value);
                      });
                      $('input[name='+coba[0]+']').focus();
                },
                      complete: function() {
                          $("#formBarangMasuk")[0].reset();
                      }
                  });
              }
              });
        }
            
        $(document).on('click', '.editBarangMasuk', function(){
            var nomor = $(this).data('id');
            $('#formEditBarangMasuk').submit('');
            $.ajax({
              url:'barangmasuk/getedit' + '/' + nomor,
              method:'get',
              data:{id:nomor},
              dataType:'json',
              success:function(data){
                console.log(data);
                state = "update";
                $('#id').val(data.id);
                $('#barang_id').val(data.barang_id);
                $('#harga_beli').val(data.harga_beli);
                $('#stok_masuk').val(data.stok_masuk);
                $('#supplier_id').val(data.supplier_id);
                $('#bm_edit').show();
                $('#bm_create').attr('hidden',true);
                  $('#myModalBarangMasuk').modal('show');
                $('#aksiEditBarangMasuk').val('Simpan');
                $('.modal-title').text('Masukan Data BarangMasuk');
                }
              });
          });

        $(document).on('hide.bs.modal','#myModalBarangMasuk', function() {
                $('#dataTableBarangMasuk').DataTable().ajax.reload();
              });
        $(document).on('click', '.deleteBarangMasuk', function(){
                var bebas = $(this).attr('id');
                  if (confirm("Yakin Dihapus ?")) {
                    $.ajax({
                      url: 'ajaxdata/removedatabarangmasuk',
                      method: "get",
                      data:{id:bebas},
                      success: function(data){
                        swal({
                          title:'Success Delete!',
                          text:'Data Berhasil Dihapus',
                          type:'success',
                          timer:'1500'
                        });
                        $('#dataTableBarangMasuk').DataTable().ajax.reload();
                      }
                    })
                  }
                  else
                  {
                    swal({
                      title:'Batal',
                      text:'Data Tidak Jadi Dihapus',
                      type:'error',
                      });
                    return false;
                  }
                });
        
});