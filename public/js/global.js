$(document).ready(function(){
    //create merk barang
    $('#TambahMasuk').attr('hidden',true);
    $('#TambahBarangMasukChange').on('click',function() {
        $('.barangMasuk').attr('hidden', true);
        $('#TambahMasuk').attr('hidden',false);
    });
    $('#TutupTambahMasuk').on('click',function(){
    	$('#TambahMasuk').attr('hidden',true);
    	$('.barangMasuk').attr('hidden',false);
    })

    $('#TambahKeluar').attr('hidden',true);
    $('#TambahBarangKeluarChange').on('click',function() {
        $('.barangKeluar').attr('hidden', true);
        $('#TambahKeluar').attr('hidden',false);
    });
    $('#TutupTambahKeluar').on('click',function(){
        $('#TambahKeluar').attr('hidden',true);
        $('.barangKeluar').attr('hidden',false);
    })
});