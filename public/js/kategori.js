  $(document).ready(function() {
    createData();
          $('#dataTableKategori').DataTable({
            processing: true,
            serverSide: true,
            ajax: '/jsonkategori',
            columns:[
                  { data: 'nama_kategori', name: 'nama_kategori' },
                  { data: 'action', orderable: false, searchable: false }
              ],
            });

          $('#TambahKategori').click(function(){
            $('#myModalKategori').modal('show');
            $('.modal-title').text('Masukan Data Kategori');
            $('#aksi').val('TambahKategori');
            state = "insert";
            });

           $('#myModalKategori').on('hidden.bs.modal',function(e){
            $(this).find('#formKategori')[0].reset();
            $('span.has-error').text('');
            $('.form-group.has-error').removeClass('has-error');
            });

        //      $('#formKategori').submit(function(e){
           //      $.ajaxSetup({
           //        header: {
           //          'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
           //        }
           //      });
        //     error.preventDefault();

        //     if (state == 'insert'){
              
        //       $.ajax({
        //         type: "POST",
        //         url: "{{url ('/storeKategori')}}",
        //         data: new FormData(this),
        //        // data: $('#student_form').serialize(),
        //         contentType: false,
        //         processData: false,
        //         dataType: 'json',
        //         success: function (data){
        //           console.log(data);
        //           swal({
        //               title:'Success Tambah!',
        //               text: data.message,
        //               type:'success',
        //               timer:'2000'
        //             });
        //           $('#myModalKategori').modal('hide');
        //           $('#dataTableKategori').DataTable().ajax.reload();
        //         },
        //         //menampilkan validasi error
        //         error: function (data){
        //           $('input').on('keydown keypress keyup click change', function(){
        //           $(this).parent().removeClass('has-error');
        //           $(this).next('.help-block').hide()
        //         });
        //           var coba = new Array();
        //           console.log(data.responseJSON.errors);
        //           $.each(data.responseJSON.errors,function(name, value){
        //             console.log(name);
        //             coba.push(name);
        //             $('input[name='+name+']').parent().addClass('has-error');
        //             $('input[name='+name+']').next('.help-block').show().text(value);
        //           });
        //           $('input[name='+coba[0]+']').focus();
        //         }
        //       });
        //     }
        // });

        function createData() {
            $('#formKategori').submit(function(e){
                $.ajaxSetup({
                  header: {
                    'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
                  }
                });
                e.preventDefault();
                if (state == 'insert') {
                $.ajax({
                    url:'storekategori',
                    type:'post',
                    data: new FormData(this),
                    cache: true,
                    contentType: false,
                    processData: false,
                    async:false,
                    dataType: 'json',
                    success: function (data){
                      console.log(data);
                      var success = "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button>"
                      success += "Data Berhasil disimpan!"
                      $('#success').addClass('alert alert-success alert-dismissible fade in').html(success);
                      //
                      $('#success').attr('hidden',false);
                      setTimeout(function(){
                        $('#success').fadeOut('slow');
                      }, 5000);
                      $('#myModalKategori').modal('hide');
                      $('#dataTableKategori').DataTable().ajax.reload();
                    },
                    error: function (data){
                      $('input').on('keydown keypress keyup click change', function(){
                      $(this).parent().removeClass('has-error');
                      $(this).next('.help-block').hide()
                    });
                      var coba = new Array();
                      console.log(data.responseJSON.errors);
                      $.each(data.responseJSON.errors,function(name, value){
                        console.log(name);
                        coba.push(name);
                        $('input[name='+name+']').parent().addClass('has-error');
                        $('input[name='+name+']').next('.help-block').show().text(value);
                      });
                      $('input[name='+coba[0]+']').focus();
                },
                    complete: function() {
                        $("#formKategori")[0].reset();
                    }
                });
            }
            else{
                $.ajax({
                    url:'kategori/edit' + '/' + $('#id').val(),
                    type:'post',
                    data: new FormData(this),
                    cache: true,
                    contentType: false,
                    processData: false,
                    async:false,
                    dataType: 'json',
                    success: function (data){
                      console.log(data);
                      swal({
                          title:'Success Edit !',
                          text: data.message,
                          type:'success',
                          timer:'2000'
                        });
                      $('#myModalKategori').modal('hide');
                      $('#dataTableKategori').DataTable().ajax.reload();
                    },
                    error: function (data){
                      $('input').on('keydown keypress keyup click change', function(){
                      $(this).parent().removeClass('has-error');
                      $(this).next('.help-block').hide()
                    });
                      var coba = new Array();
                      console.log(data.responseJSON.errors);
                      $.each(data.responseJSON.errors,function(name, value){
                        console.log(name);
                        coba.push(name);
                        $('input[name='+name+']').parent().addClass('has-error');
                        $('input[name='+name+']').next('.help-block').show().text(value);
                      });
                      $('input[name='+coba[0]+']').focus();
                },
                    complete: function() {
                        $("#formKategori")[0].reset();
                    }
                });

            }
            });
        }
        $(document).on('click', '.editKategori', function(){
            var nomor = $(this).data('id');
            $('#formKategori').submit('');
            $.ajax({
              url:'kategori/getedit' + '/' + nomor,
              method:'get',
              data:{id:nomor},
              dataType:'json',
              success:function(data){
                console.log(data);
                state = "update";
                $('#id').val(data.id);
                $('#nama_kategori').val(data.nama_kategori);
                $('#myModalKategori').modal('show');
                $('#aksiKategori').val('Simpan');
                $('.modal-title').text('Masukan Data Kategori');
                }
              });
          });

        $(document).on('hide.bs.modal','#myModalKategori', function() {
                $('#dataTableKategori').DataTable().ajax.reload();
              });
        $(document).on('click', '.deleteKategori', function(){
                var bebas = $(this).attr('id');
                  if (confirm("Yakin Dihapus ?")) {
                    $.ajax({
                      url: 'ajaxdata/removedatakategori',
                      method: "get",
                      data:{id:bebas},
                      success: function(data){
                        swal({
                          title:'Success Delete!',
                          text:'Data Berhasil Dihapus',
                          type:'success',
                          timer:'1500'
                        });
                        $('#dataTableKategori').DataTable().ajax.reload();
                      }
                    })
                  }
                  else
                  {
                    swal({
                      title:'Batal',
                      text:'Data Tidak Jadi Dihapus',
                      type:'error',
                      });
                    return false;
                  }
                });
        
});