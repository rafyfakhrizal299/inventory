$(document).ready(function() {
  createData();
	      $('#dataTableTagihan').DataTable({
            processing: true,
            serverSide: true,
            ajax: '/jsontagihan',
            columns:[
                  { data: 'id_order_detail1'},
                  { data: 'id_order_detail2'},
                  { data: 'total_pembayaran', name: 'total_pembayaran' },
                  { data: 'sisa_pembayaran', name: 'sisa_pembayaran' },
                  { data: 'jumlah_pembayaran', name: 'jumlah_pembayaran' },

                  { data: 'status'},
                  
                   { data: 'action', orderable: false, searchable: false }
              ],
            });
        function createData() {
            $('#formTagihan').submit(function(e){
                $.ajaxSetup({
                  header: {
                    'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
                  }
                });
                e.preventDefault();
                $.ajax({
                    url:'tagihan/edit' + '/' + $('#id').val(),
                    type:'post',
                    data: new FormData(this),
                    cache: true,
                    contentType: false,
                    processData: false,
                    async:false,
                    dataType: 'json',
                    success: function (data){
                      console.log(data);
                      var success = "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button>"
                      success += "Data Berhasil disimpan!"
                      $('#success').addClass('alert alert-success alert-dismissible fade in').html(success);
                      //
                      $('#success').attr('hidden',false);
                      setTimeout(function(){
                        $('#success').fadeOut('slow');
                      }, 5000);
                      $('#myModalTagihan').modal('hide');
                      $('#dataTableTagihan').DataTable().ajax.reload();
                    },
                    complete: function() {
                        $("#formTagihan")[0].reset();
                    }
                });
            });
        }
        $(document).on('click', '.editTagihan', function(){
            var nomor = $(this).data('id');
            $('#formTagihan').submit('');
            $.ajax({
              url:'tagihan/getedit' + '/' + nomor,
              method:'get',
              data:{id:nomor},
              dataType:'json',
              success:function(data){
                console.log(data);
                state = "update";
                $('#id').val(data.id);
                $('#down_payment').val(data.uang_muka);
                $('#myModalTagihan').modal('show');
                $('#aksiTagihan').val('Simpan');
                $('.modal-title').text('Keluaran Data Barang Keluar');
                }
              });
          });

        $(document).on('hide.bs.modal','#myModalTagihan', function() {
                $('#dataTableTagihan').DataTable().ajax.reload();
              });
        $(document).on('click', '.deleteTagihan', function(){
                var bebas = $(this).attr('id');
                  if (confirm("Yakin Dihapus ?")) {
                    $.ajax({
                      url: 'ajaxdata/removedatatagihan',
                      method: "get",
                      data:{id:bebas},
                      success: function(data){
                        swal({
                          title:'Success Delete!',
                          text:'Data Berhasil Dihapus',
                          type:'success',
                          timer:'1500'
                        });
                        $('#dataTableTagihan').DataTable().ajax.reload();
                      }
                    })
                  }
                  else
                  {
                    swal({
                      title:'Batal',
                      text:'Data Tidak Jadi Dihapus',
                      type:'error',
                      });
                    return false;
                  }
                });
	      

});