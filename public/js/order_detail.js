$(document).ready(function() {
	      $('#dataTableDetailOrder').DataTable({
            processing: true,
            serverSide: true,
            ajax: '/jsonorderdetail',
            columns:[
                  { data: 'kode_order', name: 'kode_order' },
                  { data: 'id_order'},
                  { data: 'id_order2'},
                  { data: 'uang_muka', name: 'uang_muka' },
                  { data: 'potongan_harga', name: 'potongan_harga' },
                  { data: 'sub_total', name: 'sub_total' },
                  { data: 'total', name: 'total' },
                  { data: 'metode_pengiriman', name: 'metode_pengiriman' },
                  
                   { data: 'action', orderable: false, searchable: false }
              ],
            });
	      

});