$(document).ready(function() {
	createData();
          $('#dataTableSupplier').DataTable({
            processing: true,
            serverSide: true,
            ajax: '/jsonsupplier',
            columns:[
                  { data: 'nama_supplier', name: 'nama_supplier' },
                  { data: 'nama_perusahaan', name: 'nama_perusahaan' },
                  { data: 'alamat', name: 'alamat' },
                  { data: 'no_telp', name: 'no_telp' },
                  { data: 'action', orderable: false, searchable: false }
              ],
            });

          $('#TambahSupplier').click(function(){
            $('#myModalSupplier').modal('show');
            $('.modal-title').text('Masukan Data Supplier');
            $('#aksiSupplier').val('TambahSupplier');
            state = "insert";
            });

           $('#myModalSupplier').on('hidden.bs.modal',function(e){
            $(this).find('#formSupplier')[0].reset();
            $('span.has-error').text('');
            $('.form-group.has-error').removeClass('has-error');
            });

        //  	$('#formSupplier').submit(function(e){
	       //      $.ajaxSetup({
	       //        header: {
	       //          'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
	       //        }
	       //      });
        //     error.preventDefault();

        //     if (state == 'insert'){
              
        //       $.ajax({
        //         type: "POST",
        //         url: "{{url ('/storeSupplier')}}",
        //         data: new FormData(this),
        //        // data: $('#student_form').serialize(),
        //         contentType: false,
        //         processData: false,
        //         dataType: 'json',
        //         success: function (data){
        //           console.log(data);
        //           swal({
        //               title:'Success Tambah!',
        //               text: data.message,
        //               type:'success',
        //               timer:'2000'
        //             });
        //           $('#myModalSupplier').modal('hide');
        //           $('#dataTableSupplier').DataTable().ajax.reload();
        //         },
        //         //menampilkan validasi error
        //         error: function (data){
        //           $('input').on('keydown keypress keyup click change', function(){
        //           $(this).parent().removeClass('has-error');
        //           $(this).next('.help-block').hide()
        //         });
        //           var coba = new Array();
        //           console.log(data.responseJSON.errors);
        //           $.each(data.responseJSON.errors,function(name, value){
        //             console.log(name);
        //             coba.push(name);
        //             $('input[name='+name+']').parent().addClass('has-error');
        //             $('input[name='+name+']').next('.help-block').show().text(value);
        //           });
        //           $('input[name='+coba[0]+']').focus();
        //         }
        //       });
        //     }
        // });

        function createData() {
	        $('#formSupplier').submit(function(e){
	            $.ajaxSetup({
	              header: {
	                'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
	              }
	            });
	            e.preventDefault();
	            if (state == 'insert') {
	            $.ajax({
	                url:'storesupplier',
	                type:'post',
	                data: new FormData(this),
	                cache: true,
	                contentType: false,
	                processData: false,
	                async:false,
	                dataType: 'json',
	                success: function (data){
	                  console.log(data);
	                  var success = "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button>"
                      success += "Data Berhasil disimpan!"
                      $('#success').addClass('alert alert-success alert-dismissible fade in').html(success);
                      //
                      $('#success').attr('hidden',false);
                      setTimeout(function(){
                        $('#success').fadeOut('slow');
                      }, 5000);
	                  $('#myModalSupplier').modal('hide');
	                  $('#dataTableSupplier').DataTable().ajax.reload();
	                },
	                error: function (data){
                      $('input').on('keydown keypress keyup click change', function(){
                      $(this).parent().removeClass('has-error');
                      $(this).next('.help-block').hide()
                    });
                      var coba = new Array();
                      console.log(data.responseJSON.errors);
                      $.each(data.responseJSON.errors,function(name, value){
                        console.log(name);
                        coba.push(name);
                        $('input[name='+name+']').parent().addClass('has-error');
                        $('input[name='+name+']').next('.help-block').show().text(value);
                      });
                      $('input[name='+coba[0]+']').focus();
                },
	                complete: function() {
	                    $("#formSupplier")[0].reset();
	                }
            	});
	        }
	        else{
	            $.ajax({
	                url:'supplier/edit' + '/' + $('#id').val(),
	                type:'post',
	                data: new FormData(this),
	                cache: true,
	                contentType: false,
	                processData: false,
	                async:false,
	                dataType: 'json',
	                success: function (data){
	                  console.log(data);
	                  swal({
	                      title:'Success Edit !',
	                      text: data.message,
	                      type:'success',
	                      timer:'2000'
	                    });
	                  $('#myModalSupplier').modal('hide');
	                  $('#dataTableSupplier').DataTable().ajax.reload();
	                },
	                complete: function() {
	                    $("#formSupplier")[0].reset();
	                }
            	});

	        }
        	});
    	}
    	$(document).on('click', '.editSupplier', function(){
            var nomor = $(this).data('id');
            $('#formSupplier').submit('');
            $.ajax({
              url:'supplier/getedit' + '/' + nomor,
              method:'get',
              data:{id:nomor},
              dataType:'json',
              success:function(data){
                console.log(data);
                state = "update";
                $('#id').val(data.id);
                $('#nama_supplier').val(data.nama_supplier);
                $('#nama_perusahaan').val(data.nama_perusahaan);
                $('#alamat').val(data.alamat);
                $('#no_telp').val(data.no_telp);
                $('#myModalSupplier').modal('show');
                $('#aksiSupplier').val('Simpan');
                $('.modal-title').text('Masukan Data Supplier');
                }
              });
          });

		$(document).on('hide.bs.modal','#myModalSupplier', function() {
	            $('#dataTableSupplier').DataTable().ajax.reload();
	          });
		$(document).on('click', '.deleteSupplier', function(){
	            var bebas = $(this).attr('id');
	              if (confirm("Yakin Dihapus ?")) {
	                $.ajax({
	                  url: 'ajaxdata/removedatasupplier',
	                  method: "get",
	                  data:{id:bebas},
	                  success: function(data){
	                    swal({
	                      title:'Success Delete!',
	                      text:'Data Berhasil Dihapus',
	                      type:'success',
	                      timer:'1500'
	                    });
	                    $('#dataTableSupplier').DataTable().ajax.reload();
	                  }
	                })
	              }
	              else
	              {
	                swal({
	                  title:'Batal',
	                  text:'Data Tidak Jadi Dihapus',
	                  type:'error',
	                  });
	                return false;
	              }
	            });
    	
});