function addrow(){
      var no = $('#item_table tr').length;
      var html = '';
      html +='<tr id="row_'+no+'">';
      html +='<td><select name="id_barang[]" class="form-control" id="barang">@foreach($barang as $data)<option value="{{$data->id}}">{{$data->nama_barang}}</option>@endforeach</select></td>';
      html +='<td><input type="number" name="kuantitas[]" class="form-control kuantitas"/></td>';
      html +='<td><input type="number" name="harga[]" class="form-control" value=""/></td>';
      html +='<td><button type="button" class="btn btn-danger btn-sm" onclick="remove('+ no +')"><i class="fa fa-minus-square"></i></button></td></tr>';
      $('#last').after(html);  
    }
    function remove(no){
      $('#row_'+no).remove();
    }